package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.conversion_data.ClassData;
import org.silbertb.proto.domainconverter.conversion_data.ConversionData;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ConversionDataCreator {

    private final ProcessingEnvironment processingEnv;
    private final ClassDataCreator classDataCreator;

    public ConversionDataCreator(ProcessingEnvironment processingEnv, ClassDataCreator classDataCreator) {
        this.processingEnv = processingEnv;
        this.classDataCreator = classDataCreator;
    }

    public ConversionData createConversionData(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        String converterName = processingEnv.getOptions().get("proto.domain.converter.name");
        if(converterName == null) {
            converterName = "org.silbertb.proto.domainconverter.generated.ProtoDomainConverter";
        }

        ConversionData.ConversionDataBuilder conversionData = ConversionData.builder()
                .generator(this.getClass().getName())
                .converterFullName(converterName);

        Map<String, List<ClassData>> protoToDomainClass = new HashMap<>();
        for(TypeElement annotation : annotations) {
            protoToDomainClass.putAll(createClassesData(annotation, roundEnv));
        }

        List<ClassData> defaultDomainClasses = getDefaultDomainClasses(protoToDomainClass);

        return conversionData
                .classesData(protoToDomainClass.values().stream().flatMap(Collection::stream).collect(Collectors.toUnmodifiableList()))
                .defaultClassesData(defaultDomainClasses)
                .build();
    }

    private Map<String, List<ClassData>> createClassesData(TypeElement annotation, RoundEnvironment roundEnv) {
        Map<String, List<ClassData>> protoToDomainClass = new HashMap<>();
        for(Element domainElement : roundEnv.getElementsAnnotatedWith(annotation)) {
            ClassData classData = classDataCreator.createClassData((TypeElement) domainElement);
            protoToDomainClass.computeIfAbsent(classData.protoFullName(), k -> new ArrayList<>()).add(classData);
        }
        return protoToDomainClass;
    }

    private List<ClassData> getDefaultDomainClasses(Map<String, List<ClassData>> protoToDomainClass) {
        Function<List<ClassData>, Optional<ClassData>> defaultDomainClassExtractor = e -> {
            if(e.size() == 1) {
                return Optional.of(e.get(0));
            }
            List<ClassData> defaults = e.stream().filter(ClassData::isDefault).collect(Collectors.toUnmodifiableList());
            if(defaults.size() > 1) {
                throw new IllegalArgumentException("More than one default class exists for proto '" + e.get(0).domainFullName() + "'.");
            } else if(defaults.size() == 1) {
                return Optional.of(defaults.get(0));
            }
            // More than one domain class, but none are marked as default.
            return Optional.empty();
        };
        return protoToDomainClass.values().stream()
                .map(defaultDomainClassExtractor)
                .flatMap(Optional::stream)
                .collect(Collectors.toUnmodifiableList());
    }
}
