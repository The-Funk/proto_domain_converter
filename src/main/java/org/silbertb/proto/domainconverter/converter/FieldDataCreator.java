package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.conversion_data.ConcreteFieldData;
import org.silbertb.proto.domainconverter.conversion_data.FieldData;
import org.silbertb.proto.domainconverter.conversion_data.OneofBaseFieldData;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FieldDataCreator {

    private final ProcessingEnvironment processingEnv;
    private final ConcreteFieldDataCreator concreteFieldDataCreator;
    private final OneofBaseDataCreator oneofBaseDataCreator;

    public FieldDataCreator(ProcessingEnvironment processingEnv,
                            ConcreteFieldDataCreator concreteFieldDataCreator,
                            OneofBaseDataCreator oneofBaseDataCreator) {
        this.processingEnv = processingEnv;
        this.concreteFieldDataCreator = concreteFieldDataCreator;
        this.oneofBaseDataCreator = oneofBaseDataCreator;
    }

    public ArrayList<FieldData> getFieldData(TypeElement domainElement, boolean withInheritedFields, boolean blacklist) {
        ArrayList<FieldData> fieldsData = new ArrayList<>();
        for(Element field : getDomainFields(domainElement, withInheritedFields)) {
            FieldData.FieldDataBuilder fieldDataBuilder = FieldData.builder();
            ConcreteFieldData concreteFieldData = concreteFieldDataCreator.createFieldData((VariableElement)field, blacklist);
            if(concreteFieldData != null) {
                fieldDataBuilder.concreteFieldData(concreteFieldData).build();
            }

            OneofBaseFieldData oneofBaseFieldData = oneofBaseDataCreator.createOneofBaseFieldData((VariableElement)field);
            if(oneofBaseFieldData != null) {
                fieldDataBuilder.oneofFieldData(oneofBaseFieldData);
            }

            if(concreteFieldData != null && oneofBaseFieldData != null) {
                throw new IllegalArgumentException("field is annotated with both 'ProtoField' and 'OneofField'. field: " + field);
            }

            if(concreteFieldData != null || oneofBaseFieldData != null) {
                fieldsData.add(fieldDataBuilder.build());
            }

        }
        return fieldsData;
    }

    private List<Element> getDomainFields(final TypeElement domainElement, boolean withInheritedFields) {
        List<Element> fields = domainElement.getEnclosedElements().stream().filter(e -> e.getKind().equals(ElementKind.FIELD)).collect(Collectors.toList());

        if(withInheritedFields) {
            if(domainElement.getSuperclass().getKind() == TypeKind.DECLARED) {
                TypeElement superclass = (TypeElement) processingEnv.getTypeUtils().asElement(domainElement.getSuperclass());
                fields.addAll(getDomainFields(superclass, true));
            }
        }
        return fields;
    }
}
