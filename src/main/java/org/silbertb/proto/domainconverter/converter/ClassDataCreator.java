package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.conversion_data.BuilderData;
import org.silbertb.proto.domainconverter.conversion_data.ClassData;
import org.silbertb.proto.domainconverter.conversion_data.FieldData;
import org.silbertb.proto.domainconverter.custom.NullMapper;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClassDataCreator {
    private final LangModelUtil langModelUtil;
    private final ConverterLogger logger;
    private final OneofBaseDataCreator oneofBaseDataCreator;
    private final ConstructorParametersDataCreator constructorParametersDataCreator;
    private final BuilderDataCreator builderDataCreator;
    private final FieldDataCreator fieldDataCreator;

    public ClassDataCreator(LangModelUtil langModelUtil,
                            ProcessingEnvironment processingEnv,
                            ProtoTypeUtil protoTypeUtil,
                            ConverterLogger logger) {
        this.langModelUtil = langModelUtil;
        this.logger = logger;
        ConcreteFieldDataCreator concreteFieldDataCreator = new ConcreteFieldDataCreator(langModelUtil, protoTypeUtil, logger);
        this.oneofBaseDataCreator = new OneofBaseDataCreator(langModelUtil, protoTypeUtil, processingEnv, logger, this);
        this.constructorParametersDataCreator = new ConstructorParametersDataCreator(concreteFieldDataCreator, oneofBaseDataCreator);
        this.builderDataCreator = new BuilderDataCreator(constructorParametersDataCreator);
        this.fieldDataCreator = new FieldDataCreator(processingEnv, concreteFieldDataCreator, oneofBaseDataCreator);
        oneofBaseDataCreator.setConstructorParametersDataCreator(constructorParametersDataCreator);
        oneofBaseDataCreator.setBuilderDataCreator(builderDataCreator);
        oneofBaseDataCreator.setFieldDataCreator(fieldDataCreator);
    }

    public ClassData createClassData(TypeElement domainElement) {
        ProtoClass protoClassAnnotation = domainElement.getAnnotation(ProtoClass.class);

        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror protoClass = langModelUtil.getClassFromAnnotation(protoClassAnnotation::protoClass);

        //deprecated way to use mapper
        String mapperFullName = getCustomMapper(protoClassAnnotation);

        boolean withInheritedFields = protoClassAnnotation.withInheritedFields();

        return createClassData(domainElement, protoClass, withInheritedFields, mapperFullName, protoClassAnnotation.blacklist());
    }

    public ClassData createClassData(TypeElement domainElement, TypeMirror protoClass, boolean withInheritedFields, boolean blacklist) {
        return createClassData(domainElement, protoClass, withInheritedFields, null, blacklist);
    }

    private ClassData createClassData(TypeElement domainElement, TypeMirror protoClass,
                                     boolean withInheritedFields,
                                     String mapperFullNameFromProtoClassAnnotation,
                                      boolean blacklist) {
        ClassData.ClassDataBuilder classDataBuilder = ClassData.builder()
                .domainFullName(domainElement.getQualifiedName().toString())
                .protoFullName(protoClass.toString());

        String mapperFullName = null;
        if(mapperFullNameFromProtoClassAnnotation == null) {
            //recommended way to use mapper
            ProtoClassMapper mapperAnnotation = domainElement.getAnnotation(ProtoClassMapper.class);
            if(mapperAnnotation != null) {
                mapperFullName = getCustomMapper(mapperAnnotation);
            }
        }

        BuilderData builderData = builderDataCreator.createClassLevelBuilderData(domainElement);

        List<FieldData> constructorParametersData =
                constructorParametersDataCreator.getConstructorParametersData(domainElement, ProtoConstructor.class, Modifier.PUBLIC);
        List<FieldData> builderConstructorParametersData = builderDataCreator.createBuilderConstructorParametersData(domainElement);

        validateAnnotations(domainElement, mapperFullName, builderData, constructorParametersData, builderConstructorParametersData);

        if(mapperFullName != null) {
            return classDataBuilder
                    .mapperFullName(mapperFullName)
                    .constructorParameters(Collections.emptyList())
                    .fieldsData(Collections.emptyList())
                    .build();
        }

        if(constructorParametersData != null) {
            classDataBuilder.constructorParameters(constructorParametersData);
        }

        if(builderConstructorParametersData != null){
            classDataBuilder.constructorParameters(builderConstructorParametersData);
            logger.info("builderConstructorParametersData size is " + builderConstructorParametersData.size() +". class: " + domainElement);
            builderData = builderDataCreator.getConstructorBuilderData(domainElement);
        }

        classDataBuilder.builderData(builderData);

        ArrayList<FieldData> fieldsData = fieldDataCreator.getFieldData(domainElement, withInheritedFields, blacklist);

        classDataBuilder.fieldsData(fieldsData);

        OneofBase oneofBaseAnnotation = domainElement.getAnnotation(OneofBase.class);
        classDataBuilder.oneofBaseClassData(oneofBaseDataCreator.createOneofBaseClassData(oneofBaseAnnotation, protoClass, blacklist));

        ProtoClassDefault protoClassDefaultAnnotation = domainElement.getAnnotation(ProtoClassDefault.class);
        if(protoClassDefaultAnnotation != null) {
            classDataBuilder.isDefault(true);
        }

        return classDataBuilder.build();
    }

    private void validateAnnotations(TypeElement domainElement,
                                     String mapperFullName,
                                     BuilderData classLevelBuilderData,
                                     List<FieldData> constructorParametersData,
                                     List<FieldData> builderConstructorParametersData) {
        if(mapperFullName != null) {
            if(classLevelBuilderData != null || builderConstructorParametersData != null) {
                throw new IllegalArgumentException("Both annotations are used:  @ProtoClassMapper and @ProtoBuilder. class: " + domainElement);
            }

            if(constructorParametersData != null) {
                throw new IllegalArgumentException("Both annotations are used:  @ProtoClassMapper and @ProtoConstructor. class: " + domainElement);
            }
        }

        if(constructorParametersData != null && builderConstructorParametersData != null) {
            throw new IllegalArgumentException("Both annotations are used on constructors:  @ProtoConstructor and @ProtoBuilder. class: " + domainElement);
        }

        if(classLevelBuilderData != null && builderConstructorParametersData != null) {
            throw new IllegalArgumentException("@ProtoBuilder annotation is used both in class context and in constructor context. class: " + domainElement);
        }
    }

    private String getCustomMapper(ProtoClass protoClassAnnotation) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror mapperClass = langModelUtil.getClassFromAnnotation(protoClassAnnotation::mapper);
        String mapperFullName = mapperClass.toString();

        if(mapperFullName.equals(NullMapper.class.getName())) {
            return null;
        }

        return mapperFullName;
    }

    private String getCustomMapper(ProtoClassMapper protoMapperAnnotation) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror mapperClass = langModelUtil.getClassFromAnnotation(protoMapperAnnotation::mapper);
        return mapperClass.toString();

    }
}
