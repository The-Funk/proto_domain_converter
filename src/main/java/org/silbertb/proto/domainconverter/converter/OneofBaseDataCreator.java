package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.conversion_data.*;
import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;
import org.silbertb.proto.domainconverter.util.StringUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OneofBaseDataCreator {

    private final LangModelUtil langModelUtil;
    private final ProtoTypeUtil protoTypeUtil;
    private final ProcessingEnvironment processingEnv;
    private final ClassDataCreator classDataCreator;
    private ConstructorParametersDataCreator constructorParametersDataCreator;
    private BuilderDataCreator builderDataCreator;
    private FieldDataCreator fieldDataCreator;
    private final ConverterLogger logger;

    public OneofBaseDataCreator(LangModelUtil langModelUtil, ProtoTypeUtil protoTypeUtil, ProcessingEnvironment processingEnv, ConverterLogger logger,
                                ClassDataCreator classDataCreator) {
        this.langModelUtil = langModelUtil;
        this.protoTypeUtil = protoTypeUtil;
        this.processingEnv = processingEnv;
        this.classDataCreator = classDataCreator;
        this.logger = logger;
    }

    public void setConstructorParametersDataCreator(ConstructorParametersDataCreator constructorParametersDataCreator) {
        this.constructorParametersDataCreator = constructorParametersDataCreator;
    }

    public void setBuilderDataCreator(BuilderDataCreator builderDataCreator) {
        this.builderDataCreator = builderDataCreator;
    }

    public void setFieldDataCreator(FieldDataCreator fieldDataCreator) {
        this.fieldDataCreator = fieldDataCreator;
    }

    public OneofBaseFieldData createOneofBaseFieldData(VariableElement field) {
        OneofBase oneofBaseAnnotation = field.getAnnotation(OneofBase.class);
        if(oneofBaseAnnotation == null) {
            return null;
        }

        String domainFieldName = field.getSimpleName().toString();
        String oneofBaseField = StringUtils.capitalize(field.getSimpleName().toString());
        OneofBaseFieldData.OneofBaseFieldDataBuilder oneofBaseFieldData = OneofBaseFieldData.builder();
        oneofBaseFieldData
                .oneofProtoName(
                        oneofBaseAnnotation.oneofName().equals("") ?
                                StringUtils.capitalize(field.getSimpleName().toString()) :
                                StringUtils.snakeCaseToPascalCase(oneofBaseAnnotation.oneofName()))
                .domainFieldName(domainFieldName)
                .oneofBaseField(oneofBaseField)
                .oneOfFieldsData(createOneofFieldDataList(oneofBaseAnnotation, oneofBaseField))
                .domainFieldType(field.asType().toString());


        return oneofBaseFieldData.build();
    }

    public OneofBaseClassData createOneofBaseClassData(OneofBase oneofBaseAnnotation, TypeMirror protoClass, boolean blacklist) {
        if(oneofBaseAnnotation == null) {
            return null;
        }

        return OneofBaseClassData.builder()
                .oneofProtoName(StringUtils.snakeCaseToPascalCase(oneofBaseAnnotation.oneofName()))
                .oneOfFieldsData(createOneofFieldDataForClassList(oneofBaseAnnotation, protoClass, blacklist))
                .build();
    }

    private List<OneofFieldDataForClass> createOneofFieldDataForClassList(OneofBase oneofBaseAnnotation, TypeMirror protoClass, boolean blacklist) {
        OneofFieldComparator oneofFieldComparator = new OneofFieldComparator(processingEnv, langModelUtil);
        return Arrays.stream(oneofBaseAnnotation.oneOfFields())
                .sorted(oneofFieldComparator)
                .map(oneofField -> createOneofFieldDataForClass(oneofField, oneofFieldComparator.getType(oneofField), protoClass, blacklist))
                .collect(Collectors.toList());
    }

    private OneofFieldDataForClass createOneofFieldDataForClass(OneofField oneofFieldAnnotation, TypeMirror domainType, TypeMirror protoClass, boolean blacklist) {
        TypeElement domainElement = (TypeElement)processingEnv.getTypeUtils().asElement(domainType);
        ProtoClass oneofBaseAnnotation = domainElement.getAnnotation(ProtoClass.class);
        boolean isMessage = oneofBaseAnnotation != null;

        ClassData classData = isMessage ? classDataCreator.createClassData(domainElement) :
                classDataCreator.createClassData(domainElement, protoClass, true, blacklist);


        String protoField = oneofFieldAnnotation.protoField();
        return OneofFieldDataForClass.builder()
                .protoField(protoField)
                .classData(classData)
                .isMessage(isMessage)
                .build();
    }

    private List<OneofFieldData> createOneofFieldDataList(OneofBase oneofBaseAnnotation, String oneofBaseField) {
        OneofFieldComparator oneofFieldComparator = new OneofFieldComparator(processingEnv, langModelUtil);
        return Arrays.stream(oneofBaseAnnotation.oneOfFields())
                .sorted(oneofFieldComparator)
                .map(oneofField -> createOneofFieldData(oneofField, oneofFieldComparator.getType(oneofField), oneofBaseField))
                .collect(Collectors.toList());
    }

    private OneofFieldData createOneofFieldData(OneofField oneofFieldAnnotation, TypeMirror domainType, String oneofBaseField) {
        VariableElement fieldElement = getFieldElement(oneofFieldAnnotation, domainType, oneofBaseField);
        String converterFullName = fieldElement == null ? getConverterFullName(oneofFieldAnnotation, domainType, null) : null;

        TypeElement domainTypeElement = (TypeElement) processingEnv.getTypeUtils().asElement(domainType);
        List<FieldData> constructorParametersData =
                constructorParametersDataCreator.getConstructorParametersData(domainTypeElement, ProtoConstructor.class, Modifier.PUBLIC);
        BuilderData builderData = builderDataCreator.getConstructorBuilderData(domainTypeElement);
        if(builderData != null) {
            if(constructorParametersData != null) {
                throw new RuntimeException(domainTypeElement + " has both @ProtoConstructor and @ProtoBuilder defined");
            }

            constructorParametersData = builderDataCreator.createBuilderConstructorParametersData(domainTypeElement);
        }

        ConcreteFieldData constructorParameter = null;
        if(constructorParametersData != null) {
            if(constructorParametersData.size() != 1) {
                throw new RuntimeException("There should be only one constructor parameter for " + domainTypeElement);
            }

            FieldData param = constructorParametersData.get(0);
            if(param.oneofFieldData() != null) {
                throw new RuntimeException("There shouldn't be constructor parameter for " + domainTypeElement + " annotated with @OneofBase since this class is oneof implementation itself.");
            }
            if(!param.concreteFieldData().protoFieldPascalCase().equals(
                    StringUtils.snakeCaseToPascalCase(oneofFieldAnnotation.protoField()))) {
                throw new RuntimeException("Constructor parameter for " + domainTypeElement + " should correlate proto field " + oneofFieldAnnotation.protoField());
            }

            constructorParameter = param.concreteFieldData();
        }

        BuilderData classBuilderData = builderDataCreator.createClassLevelBuilderData(domainTypeElement);
        if(classBuilderData != null) {
            if(builderData != null) {
                throw new RuntimeException(domainTypeElement + " has @ProtoBuilder both on class level and on constructor");
            }
            builderData = classBuilderData;
        }

        boolean domainTypeIsMessage = protoTypeUtil.isProtoMessage(processingEnv.getElementUtils().getTypeElement(domainType.toString()).asType());
        ConcreteFieldData concreteFieldData = domainTypeIsMessage ? null : getConcreteField(oneofFieldAnnotation, oneofBaseField);

        return OneofFieldData.builder()
                .protoField(oneofFieldAnnotation.protoField())
                .oneofImplClass(domainType.toString())
                .converterFullName(converterFullName)
                .constructorParameter(constructorParameter)
                .builderData(builderData)
                .fieldData(concreteFieldData)
                .build();
    }

    private ConcreteFieldData getConcreteField(OneofField oneofFieldAnnotation, String oneofBaseField) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror domainType = langModelUtil.getClassFromAnnotation(oneofFieldAnnotation::domainClass);

        ConcreteFieldData concreteFieldDataUsingDomain = getConcreteFieldUsingDomainField(oneofFieldAnnotation, oneofBaseField, domainType);
        ConcreteFieldData concreteFieldDataUsingProtoField = getConcreteFieldUsingProtoField(oneofFieldAnnotation, domainType);

        if(concreteFieldDataUsingDomain == null) {
            return concreteFieldDataUsingProtoField;
        }

        if(concreteFieldDataUsingProtoField == null) {
            return concreteFieldDataUsingDomain;
        }
        if(concreteFieldDataUsingDomain.equals(concreteFieldDataUsingProtoField)) {
            return concreteFieldDataUsingProtoField;
        }

        throw new RuntimeException(domainType + ": Class member annotated with @ProtoField (" + concreteFieldDataUsingProtoField +
                ") if different than the domaing field in @OneofField  ("+ concreteFieldDataUsingDomain + ")");
    }

    private ConcreteFieldData getConcreteFieldUsingDomainField(OneofField oneofFieldAnnotation, String oneofBaseField, TypeMirror domainType) {
        VariableElement fieldElement = getFieldElement(oneofFieldAnnotation, domainType, oneofBaseField);
        if(fieldElement == null) {
            return null;
        }
        String converterFullName = getConverterFullName(oneofFieldAnnotation, domainType, fieldElement);
        ProtoType protoTypeForConverter = converterFullName == null ? null : ProtoType.OTHER;
        String oneofDomainField = oneofFieldAnnotation.domainField();
        String oneofDomainFieldType = fieldElement.asType().toString();
        ConcreteFieldData.ConcreteFieldDataBuilder fieldData = ConcreteFieldData.builder();
        FieldType fieldType = protoTypeUtil.calculateFieldType(fieldElement.asType());
        TypeMirror elementType = protoTypeUtil.getElementType(fieldElement.asType(), fieldType);

        return fieldData.domainFieldName(oneofDomainField)
                .domainTypeFullName(oneofDomainFieldType)
                .explicitProtoFieldName(oneofFieldAnnotation.protoField())
                .fieldType(fieldType)
                .domainItemTypeFullName(elementType == null ? null : elementType.toString())
                .dataStructureConcreteType(protoTypeUtil.calculateDataStructureConcreteType(fieldElement))
                .converterFullName(converterFullName)
                .protoTypeForConverter(protoTypeForConverter)
                .build();
    }

    private String getConverterFullName(OneofField oneofFieldAnnotation, TypeMirror domainType, VariableElement fieldElement) {

        ProtoConverter protoConverterAnnotation =
                fieldElement == null ? processingEnv.getTypeUtils().asElement(domainType).getAnnotation(ProtoConverter.class) : fieldElement.getAnnotation(ProtoConverter.class);

        if(protoConverterAnnotation == null) {
            return null;
        }

        if(!protoConverterAnnotation.protoType().equals(ProtoType.OTHER)) {
            throw new RuntimeException("protoType for @ProtoConverter on a 'oneof' field must be 'OTHER'. oneof base field: " +
                    fieldElement +
                    " class: " + domainType +
                    " field: " + oneofFieldAnnotation.domainField());
        }

        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror converterType = langModelUtil.getClassFromAnnotation(protoConverterAnnotation::converter);
        return converterType.toString();
    }

    private VariableElement getFieldElement(OneofField oneofFieldAnnotation, TypeMirror domainType, String oneofBaseField) {
        if(oneofFieldAnnotation.domainField().isEmpty()) {
            return null;
        }

        VariableElement fieldElement = langModelUtil.getMemberField(domainType, oneofFieldAnnotation.domainField());
        if(fieldElement == null) {
            throw new RuntimeException("OneofField annotation on field '" + oneofBaseField +
                    "' declared non-existing field for class '" + domainType + "': " +
                    oneofFieldAnnotation.domainField());
        }
        return fieldElement;
    }

    private ConcreteFieldData getConcreteFieldUsingProtoField(OneofField oneofFieldAnnotation, TypeMirror domainType) {
        TypeElement domainElement = (TypeElement) processingEnv.getTypeUtils().asElement(domainType);
        List<FieldData> fieldsData = fieldDataCreator.getFieldData(domainElement, true, false);
        if(fieldsData.isEmpty()) {
            String protoFieldPascalCase = StringUtils.snakeCaseToPascalCase(oneofFieldAnnotation.protoField());
            fieldsData = fieldDataCreator.getFieldData(domainElement, true, true).stream()
                    .filter(f -> f.concreteFieldData() != null && f.concreteFieldData().protoFieldPascalCase().equals(protoFieldPascalCase))
                    .collect(Collectors.toList());
        }
        return getConcreteField(fieldsData, "class member", domainElement, oneofFieldAnnotation);
    }

    private ConcreteFieldData getConcreteField(List<FieldData> fieldsData, String fieldType, TypeElement domainElement, OneofField oneofFieldAnnotation) {
        if(fieldsData.isEmpty()) {
            return null;
        }

        if(fieldsData.size() != 1) {
            throw new RuntimeException("There should be only one " + fieldType + " for " + domainElement);
        }

        FieldData param = fieldsData.get(0);
        if(param.oneofFieldData() != null) {
            throw new RuntimeException("There shouldn't be " + fieldType + " for " + domainElement + " annotated with @OneofBase since this class is oneof implementation itself.");
        }

        if(!param.concreteFieldData().protoFieldPascalCase().equals(
                StringUtils.snakeCaseToPascalCase(oneofFieldAnnotation.protoField()))) {
            throw new RuntimeException(fieldType + " for " + domainElement + " should correlate proto field " + oneofFieldAnnotation.protoField());
        }

        return param.concreteFieldData();
    }
}
