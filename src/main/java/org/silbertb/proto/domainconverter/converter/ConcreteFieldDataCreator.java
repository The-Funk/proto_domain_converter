package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoIgnore;
import org.silbertb.proto.domainconverter.conversion_data.FieldType;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.conversion_data.ConcreteFieldData;

import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

public class ConcreteFieldDataCreator {

    private final LangModelUtil langModelUtil;
    private final ProtoTypeUtil protoTypeUtil;
    private ConverterLogger logger;

    public ConcreteFieldDataCreator(LangModelUtil langModelUtil, ProtoTypeUtil protoTypeUtil, ConverterLogger logger) {
        this.langModelUtil = langModelUtil;
        this.protoTypeUtil = protoTypeUtil;
        this.logger = logger;
    }

    public ConcreteFieldData createFieldData(VariableElement field, boolean blacklist) {
        ProtoField protoFieldAnnotation = field.getAnnotation(ProtoField.class);
        ProtoIgnore protoIgnoreAnnotation = field.getAnnotation(ProtoIgnore.class);
        if(protoIgnoreAnnotation != null || (!blacklist && protoFieldAnnotation == null)) {
            if(protoFieldAnnotation != null) {
                logger.info("Both @ProtoField and @ProtoIgnore annotate " + field.getSimpleName() + ". Ignoring");
            }
            return null;
        }

        String explicitProtoName = protoFieldAnnotation == null ? "" : protoFieldAnnotation.protoName();
        return createFieldData(field, explicitProtoName);
    }

    public ConcreteFieldData createFieldData(VariableElement field, String explicitProtoFieldName) {
        TypeMirror domainType = field.asType();
        ConcreteFieldData.ConcreteFieldDataBuilder fieldData = ConcreteFieldData.builder();
        FieldType fieldType = protoTypeUtil.calculateFieldType(domainType);
        TypeMirror elementType = protoTypeUtil.getElementType(domainType, fieldType);

        fieldData.domainFieldName(field.getSimpleName().toString())
                .domainTypeFullName(domainType.toString())
                .domainItemTypeFullName(elementType == null ? null : elementType.toString())
                .explicitProtoFieldName(explicitProtoFieldName)
                .fieldType(fieldType)
                .dataStructureConcreteType(protoTypeUtil.calculateDataStructureConcreteType(field));

        ProtoConverter protoConverterAnnotation = field.getAnnotation(ProtoConverter.class);
        if(protoConverterAnnotation != null) {
            @SuppressWarnings("ResultOfMethodCallIgnored")
            TypeMirror converterType = langModelUtil.getClassFromAnnotation(protoConverterAnnotation::converter);

            fieldData.protoTypeForConverter(protoConverterAnnotation.protoType())
                    .converterFullName(converterType.toString());
        }

        return fieldData.build();
    }

}
