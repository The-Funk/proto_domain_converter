package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

import java.util.Collection;
import java.util.List;

@Accessors(fluent = true)
@Getter

public class ConversionData {
    private final String generator;
    private final String converterPackage;
    private final String converterClass;
    private final List<ClassData> classesData;
    private final Collection<ClassData> defaultClassesData;

    @Builder
    public ConversionData(String generator, String converterFullName, List<ClassData> classesData, Collection<ClassData> defaultClassesData) {
        this.generator = generator;
        this.converterPackage = StringUtils.getPackage(converterFullName);
        this.converterClass = StringUtils.getSimpleName(converterFullName);
        this.classesData = classesData;
        this.defaultClassesData = defaultClassesData;
    }
}
