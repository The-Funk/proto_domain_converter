package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
@ToString

public class FieldData {
    final private ConcreteFieldData concreteFieldData;
    final private OneofBaseFieldData oneofFieldData;
    final private boolean isLast;

    @Builder
    public FieldData(ConcreteFieldData concreteFieldData, OneofBaseFieldData oneofFieldData, boolean isLast) {
        this.concreteFieldData = concreteFieldData;
        this.oneofFieldData = oneofFieldData;
        this.isLast = isLast;

        if(concreteFieldData != null && oneofFieldData != null) {
            throw new IllegalArgumentException("field is annotated with both 'ProtoField' and 'OneofField'. concreteFieldData: " + concreteFieldData + " oneofFieldData:" + oneofFieldData);
        }
    }
}
