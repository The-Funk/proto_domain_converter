package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class OneofBaseClassData {
    private final String oneofProtoName;
    private final List<OneofFieldDataForClass> oneOfFieldsData;
}
