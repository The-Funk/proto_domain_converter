package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

import java.util.List;

@Accessors(fluent = true)
@Getter
public class ClassData {
    final private String domainFullName;
    final private String domainClass;
    final private String protoFullName;
    final private String protoClass;
    final private String mapperClass;
    final private String mapperFullName;
    final private OneofBaseClassData oneofBaseClassData;
    final private List<FieldData> fieldsData;
    final private List<FieldData> constructorParameters;
    final private BuilderData builderData;
    final private boolean isDefault;

    @Builder
    public ClassData(String domainFullName,
                     String protoFullName,
                     String mapperFullName,
                     OneofBaseClassData oneofBaseClassData,
                     List<FieldData> fieldsData,
                     List<FieldData> constructorParameters,
                     BuilderData builderData,
                     boolean isDefault) {

        this.domainFullName = domainFullName;
        this.protoFullName = protoFullName;
        this.mapperFullName = mapperFullName;
        this.oneofBaseClassData = oneofBaseClassData;
        this.fieldsData = fieldsData;
        this.constructorParameters = constructorParameters;

        this.domainClass = StringUtils.getSimpleName(domainFullName);
        this.protoClass = StringUtils.getSimpleName(protoFullName);

        this.mapperClass = mapperFullName == null ? null : StringUtils.getSimpleName(mapperFullName);
        this.builderData = builderData;

        this.isDefault = isDefault;
    }

    public boolean useBuilder() {
        return builderData != null;
    }

    public boolean hasMapper() {
        return mapperClass != null;
    }

}
