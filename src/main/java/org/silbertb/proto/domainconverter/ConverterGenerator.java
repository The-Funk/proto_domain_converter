package org.silbertb.proto.domainconverter;

import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.conversion_data.ConversionData;
import org.silbertb.proto.domainconverter.converter.*;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.Set;

@SupportedAnnotationTypes({"org.silbertb.proto.domainconverter.annotations.ProtoClass"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class ConverterGenerator extends AbstractProcessor {
    private SourceWriter sourceWriter;
    private ConverterLogger logger;
    private ConversionDataCreator conversionDataCreator;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        LangModelUtil langModelUtil = new LangModelUtil(processingEnv);
        sourceWriter = new SourceWriter(processingEnv);
        ProtoTypeUtil protoTypeUtil = new ProtoTypeUtil(processingEnv, langModelUtil);

        logger = new ConverterLogger(processingEnv);

        ClassDataCreator classDataCreator = new ClassDataCreator(
                langModelUtil,
                processingEnv,
                protoTypeUtil, logger
        );
        conversionDataCreator = new ConversionDataCreator(processingEnv, classDataCreator);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        ConversionData conversionData = conversionDataCreator.createConversionData(annotations, roundEnv);
        if(conversionData.classesData().isEmpty()) {
            return true;
        }

        // Write source file
        try {
            sourceWriter.writeSource(conversionData);
        } catch (IOException ex) {
            logger.error("Failed to generate proto domain conversion class. Exception: " + ex);
        }

        return true;
    }
}
