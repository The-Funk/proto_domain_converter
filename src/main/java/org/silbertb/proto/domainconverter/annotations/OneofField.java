package org.silbertb.proto.domainconverter.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Maps a field in a protobuf oneof group to corresponding field/class in the domain objects.
 * Used with conjunction of {@link OneofBase}
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface OneofField {
    /**
     *
     * @return the name of the protobuf field
     */
    String protoField();

    /**
     *
     * @return The domain class of this field
     */
    Class<?> domainClass();

    /**
     * @deprecated use {@link ProtoField} annotation
     * The name of the domain field. Used when the domain class is not mapped by itself to the protobuf field but have a field which is mapped to it.
     * @return The name of the domain field
     */
    @Deprecated
    String domainField() default "";
}
