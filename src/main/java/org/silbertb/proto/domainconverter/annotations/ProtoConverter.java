package org.silbertb.proto.domainconverter.annotations;

import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.custom.TypeConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declare a converter between a protobuf field and a domain field
 */
@Target(value = {ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoConverter {
    /**
     *
     * @return The converter
     */
    Class<? extends TypeConverter<?, ?>> converter();

    /**
     * The type of the protobuf field. Need to be explicitly defined if it is 'list' or 'map'
     * @return The type of the protobuf field
     */
    ProtoType protoType() default ProtoType.OTHER;
}
