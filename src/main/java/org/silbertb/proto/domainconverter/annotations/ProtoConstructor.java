package org.silbertb.proto.domainconverter.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used with conjunction of {@link ProtoField}.
 * It annotates a constructor which has all of its parameters annotated with {@link ProtoField}.
 * Useful for immutable classes rather than POJOs
 */
@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoConstructor {
}
