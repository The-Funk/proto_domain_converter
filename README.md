# proto_domain_converter
## Table of Content
* [About the Project](#about-the-project)
* [Getting the Library](#getting-the-library)
* [How to use it?](#how-to-use-it-)
* [Mapping](#mapping)
    * [Basic](#basic)
    * [Blacklist vs Whitelist](#blacklist-vs-whitelist)
    * [Different Field Names](#different-field-names)
    * [Field Value Manipulation](#field-value-manipulation)
    * [Custom conversion for a whole class](#custom-conversion-for-a-whole-class)
    * [Constructor Mapping](#constructor-mapping)
    * [Builder Mapping](#builder-mapping)
        * [Base builder on class members](#base-builder-on-class-members)
        * [Base builder on a constructor](#base-builder-on-a-constructor)
    * [Polymorphism and "oneof"](#polymorphism-and-oneof)
        * [Regular Fields - No polymorphism](#regular-fields-no-polymorphism)
        * [Map between a message with 'oneof' to a base class and sub-classes](#map-between-a-message-with-oneof-to-a-base-class-and-sub-classes)
        * [Map oneof groups to a base class field](#map-oneof-groups-to-a-base-class-field)
        * [Message with multiple oneof groups](#oneof-field-value-manipulation)
    * [Multiple domain classes to a single protobuf message](#multiple-domain-classes-to-a-single-protobuf-message)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
    

## About the Project
Protobuf is a great technology for modeling objects, serialize them and use the in api. 
One of best  feature of this technology is its robust code generation. 
It enables all stakeholders of the api to use it as intended and minimize mistakes.
However, code generation introduces a new problems.
* it binds the domain model implementation to the generated code
* It is not possible to add logic to the generated object
* using the generated code might not fit all usages. For example use ORM annotations

In order to bypass this the application usually use its own objects and convert the generated code into the application objects.
This translation involves a lot of boilerplate and prune to errors.

The purpose of this library is to enable easy and efficient conversion, with minimal boilerplate and errors.

This library is inspired by proto-converter: <https://github.com/BAData/protobuf-converter>.
The main difference is that it uses annotation in compile time rather than runtime reflection and therefore has much better performance.

## Getting the Library
In addition to the regular dependencies it is also required add annotation processing section.

gradle:
```groovy
dependencies {
    implementation 'io.gitlab.protobuf-tools:proto-domain-converter:1.2.0'
    annotationProcessor 'io.gitlab.protobuf-tools:proto-domain-converter:1.2.0'
}
```

maven:
```xml
<dependencies>
    <dependency>
        <groupId>io.gitlab.protobuf-tools</groupId>
        <artifactId>proto-domain-converter</artifactId>
        <version>1.2.0</version>
    </dependency>
</dependencies>
<build>
    <pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>io.gitlab.protobuf-tools</groupId>
                            <artifactId>proto-domain-converter</artifactId>
                            <version>1.2.0</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
        </plugins>
    </pluginManagement>
</build>
```

## How to use it ?
An example for usage can be found in [_Proto Service Example_](https://gitlab.com/protobuf-tools/proto-service-example) repo.  
Detailed explanation below.

There are several annotations which map between the application domain objects and the protobuf generated objects. 
These annotations should be added to the domain classes. They map the domain definitions to the corresponding protobuf definitions.  
These annotations are processed during pre-compilation, and a new class is generated: *org.silbertb.proto.domainconverter.generated.ProtoDomainConverter*.

This class has "*toProto*" and "*toDomain*" methods for each annotated class. In your application you should use these methods for the conversion.

Code for conversion User instance into related protobuf message:
```java
User userDomain = new User();
...
UserProto userProto = ProtoDomainConverter.toProto(userDomain);
```
Code for backward conversion:
```java
User userDomain = ProtoDomainConverter.toDomain(userProto);
```

## Mapping
### Basic
The most important annotations are [_@ProtoClass_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoClass.java) and [_@ProtoField_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoField.java)

_@ProtoClass_ maps between the domain class and the protobuf generated class.
_@ProtoField_ maps between the domain field or constructor parameter and a corresponding protobuf field within the mapped class. 
It is assumed that the domain class has standard getters and setter to this field. If _@ProtoField_ annotated a constructor parameter then there is no need for a setter.

#### Example:

##### Domain
```java
@ProtoClass(protoClass = StringProto.class)
public class StringDomain {
    @ProtoField
    private String stringValue;
}
```
##### Protobuf
```protobuf
message StringProto {
    string string_value = 1;
}
```

### Blacklist vs. Whitelist
Annotating fields with _@ProtoField_ is "whitelist". It is also possible to work in "blacklist" mode, in which case all the class members are mapped to protobuf fields, as if they are annotated with _@ProtoField_, unless they are annotated with [_@ProtoIgnore_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoIgnore.java).

#### Example:

##### Domain
```java
@ProtoClass(protoClass = StringProto.class, , blacklist = true)
public class StringDomain {
    private String stringValue;
}
```
##### Protobuf
```protobuf
message StringProto {
    string string_value = 1;
}
```

### Different Field Names
If the field names don't match it is possible to specify a name.

##### Domain
```java
@ProtoClass(protoClass = StringProto.class)
public class StringDomain {
    @ProtoField(protoName = "some_value")
    private String stringValue;
}
```
##### Protobuf
```protobuf
message StringProto {
    string some_value = 1;
}
```
### Field Value Manipulation
Sometime it is desired to apply some manipulation on the field value before assigning the value.
It is possible to do by implementing the interface [_TypeConverter_](./src/main/java/org/silbertb/proto/domainconverter/custom/TypeConverter.java) and using the annotation [_@ProtoConverter_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoConverter.java).
_TypeConverter_ handles the conversion logic.
_@ProtoConverter_ applies _TypeConverter_ to selected fields.

#### Example

##### Converter
```java
public class IntStrConverter implements TypeConverter<String, Integer> {
    @Override
    public String toDomainValue(Integer protoValue) {
        return protoValue.toString();
    }

    @Override
    public boolean shouldAssignToProto(String domainValue) {
        return domainValue != null;
    }

    @Override
    public Integer toProtobufValue(String domainValue) {
        return Integer.parseInt(domainValue);
    }
}
```
##### Domain
```java
@ProtoClass(protoClass = IntProto.class)
public class StringDomain {
    @ProtoField(protoName = "int_val")
    @ProtoConverter(converter = IntStrConverter.class)
    private String strVal;
}
```
##### Protobuf
```protobuf
message IntProto {
    int32 int_val = 1;
}
```

If the protobuf type is list or map while the domain type isn't then it is required to explicitly specify it in the _@ProtoConverter_ annotation.
#### Example

##### Domain
```java
@ProtoClass(protoClass = IntListProto.class)
public class CommaSeparatedStringDomain {
    @ProtoField(protoName = "int_list")
    @ProtoConverter(converter = IntListToCommaSeparatedStringConverter.class, protoType = ProtoType.LIST)
    private String commaSeparatedInt;
}
```
##### Protobuf
```protobuf
message IntListProto {
    repeated int32 int_list = 1;
}
```

### Custom conversion for a whole class
Sometime there might not be a one-to-one mapping between the a domain field to protobuf field. In this case there is a need to apply a custom conversion logic in the class level. It can be done by giving the interface [_Mapper_](./src/main/java/org/silbertb/proto/domainconverter/custom/Mapper.java) as a parameter to the annotation [_@ProtoClassMapper_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoClassMapper.java).

#### Example

##### Protobuf
```protobuf
message TransportProto{
    enum Protocol {
        TCP = 0;
        UDP = 1;
    }

    Protocol protocol = 1;
}
```

##### Domain
```java
@ProtoClassMapper(mapper = TransportMapper.class)
@ProtoClass(protoClass = TransportProto.class, mapper = TransportMapper.class)
public class TransportDomain {
    private boolean isTcp;
    private boolean isUdp;

    public void setUdp(boolean udp) {
        isUdp = udp;
        isTcp = !udp;
    }

    public void setTcp(boolean tcp) {
        isTcp = tcp;
        isUdp = !tcp;
    }
}

public class TransportMapper implements Mapper<TransportDomain, TransportProto> {
    @Override
    public TransportDomain toDomain(TransportProto protoValue) {
       ...
    }

    @Override
    public TransportProto toProto(TransportDomain domainValue) {
       ...
    }
}
```

### Constructor Mapping
While annotating class members is good for Java Beans or POJOs it is not recomended for more complex objects where encapsulation is recomended or when you want immutable objects. For these use cases it is possible to map the protobuf fields to constructor parameters.

All the kinds of mappings which are possible using field mapping are possible also in constructor mapping.
The same annotations that are attached to a field can be attached to a constructor parameter.

It is required to put [_@ProtoConstructor_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoConstructor.java) before the constructor. Only one constructor can be annotation with _@ProtoConstructor_, and it has to be public.

If a constructor parameter is not annotated with _@ProtoField_ or _@OneofBase_ then it behaves the same as an empty _@ProtoField_.

#### Example:

##### Domain
```java
@ProtoClass(protoClass = StringProto.class)
public class StringDomain {

  private String stringValue;

  @ProtoConstructor
  public StringDomain(@ProtoField String stringValue) {
    this.stringValue = stringValue;    
  }
    
}
```
##### Protobuf
```protobuf
message StringProto {
    string string_value = 1;
}
```

### Builder Mapping
Sometimes using constructors to create objects becomes too complex and developers choose to use the [_Builder_](https://www.digitalocean.com/community/tutorials/builder-design-pattern-in-java) design pattern. In order to address this use case it is possible to map protobuf fields to builder. 

Since builders can be implemented in various ways we had to balance between flexibility of the mapping and the complexity of it. Therefore we tried to conform with popular conventions, and particulary to the builder implementation of [_lombok_](https://projectlombok.org/features/Builder)

The mapping is done using [_@ProtoBuilder_](./src/main/java/org/silbertb/proto/domainconverter/annotations/ProtoBuilder.java) annotation. This annotation enables customizing the builder method (default is _builder()_), build method (default is _build()_) and prefix to the setter method (default is no prefix, and the method is equal to the fields name).

#### Base builder on class members
If you want to base the builder on the class members then put the _@ProtoBuilder_ annotation on the class definitions together with _@ProtoClass_. The maaping of the fields will be the same as for setters.

##### Example:

###### Domain
```java
@ProtoBuilder
@ProtoClass(protoClass = ExampleProto.class)
public class ExampleDomain {

    @ProtoField
    private String stringValue;

    @ProtoField
    private int intValue;

  private ExampleDomain(String stringValue, int intValue) {
    this.stringValue = stringValue;
    this.intValue = intValue;    
  }

  public static ExampleDomainBuilder builder() {
      return new ExampleDomainBuilder()
  }

  public static class ExampleDomainBuilder {
    private String stringValue;
    private int intValue;

    public ExampleDomainBuilder stringValue(String stringValue) {
        this.stringValue = stringValue;
        return this;
    }

    public ExampleDomainBuilder stringValue(String stringValue) {
        this.intValue = intValue;
        return this;
    }

    public ExampleDomain build() {
        return new ExampleDomain(stringValue, intValue);
    }

  }
    
}
```
###### Protobuf
```protobuf
message ExampleProto {
    string string_value = 1;
    int32 int_value = 2;
}
```

#### Base builder on a constructor
If you want to base the builder on constructor parameters then put the _@ProtoBuilder_ annotation on the desired constructor, the same way you would use _@ProtoConstructor_. 
It is not possible to use both _@ProtoBuilder_ and _@ProtoConstructor_ and only one constructor can be annotated.

##### Example:

###### Domain
```java
@ProtoClass(protoClass = ExampleProto.class)
public class ExampleDomain {

  private String stringValue;
  private int intValue;

  @ProtoBuilder
  private ExampleDomain(String stringValue, int intValue) {
    this.stringValue = stringValue;
    this.intValue = intValue;    
  }

  public static ExampleDomainBuilder builder() {
      return new ExampleDomainBuilder()
  }

  public static class ExampleDomainBuilder {
    private String stringValue;
    private int intValue;

    public ExampleDomainBuilder stringValue(String stringValue) {
        this.stringValue = stringValue;
        return this;
    }

    public ExampleDomainBuilder stringValue(String stringValue) {
        this.intValue = intValue;
        return this;
    }

    public ExampleDomain build() {
        return new ExampleDomain(stringValue, intValue);
    }

  }
    
}
```

### Polymorphism and "oneof"
Java as an object oriented language embraces polymorphism, either by using interfaces or class inheritance.
Protobuf, as api targeted language, doesn't use inheritance. Instead there is the concept of "oneof" - a group of fields in which only one can have a value. A protobuf message can have multiple 'oneof' field groups.  
Therefore there is a flexibility in the mapping. The options are:
* Treat oneof fields as regular fields. The domain class is responsible in its own logic to make sure that only one of the fields has value.
* Map a message which has only one oneof group to an interface and a set of implementing classes
* Map a message which has only one oneof group and some additional classes to a base class and sub-classes
* Map a message which has multiple oneof groups to class which contains multiple fields, each one is an interface or a base class

This flexibility is achieved by introducing two more annotations which can be used either in the class level or the field level.

[OneofBase](./src/main/java/org/silbertb/proto/domainconverter/annotations/OneofBase.java) map between a base class or interface to a oneof group. It contains a list of [OneofField](./src/main/java/org/silbertb/proto/domainconverter/annotations/OneofField.java).

[OneofField](./src/main/java/org/silbertb/proto/domainconverter/annotations/OneofField.java) maps between an imlpementing class or sub-class to the corresponding field in a oneof group.

Here are some examples for each one of these cases.

#### Regular Fields - No polymorphism
Here the 'oneof' fields are mapped to regular fields in a single domain class without any polymorphism.

##### Domain
```java
@ProtoClass(protoClass = IntOrStringProto.class)
public class IntOrStringDomain {

    @ProtoField
    private int intVal;

    @ProtoField
    private String strVal;

    public void setIntVal(int intVal) {
        this.intVal = intVal;
        this.strVal= null;
    }

    public void setStrVal(String strVal) {
        this.strVal = strVal;
        this.intVal = 0;
    }
}
```
##### Protobuf
```protobuf
message IntOrStringProto{
    oneof value {
        int32 int_val = 1;
        string str_val = 2;
    }
}
```
#### Map between a message with 'oneof' to a base class and sub-classes

When _@OneofBase_ and _@OneofField_ are placed, together with _@ProtoClass_, above a class or interface, then this class is a base class and each _OneofField_ map links between a field and a sub-class. The sub-class behaves as if it has the same _@ProtoClass_ annotation as the base class. It may have all the other mapping annotations.

##### Protobuf
```proto
message OneofSegmentProto {
    string name = 1;
    oneof value {
        int32 point = 2;
        string range = 3;
    }
}
```

##### Domain
```java
@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "point", domainClass = OneofSegmentPoint.class),
        @OneofField(protoField = "range", domainClass = OneofSegmentRange.class)
})
@ProtoClass(protoClass = OneofSegmentProto.class)
public class OneofSegmentDomain {
    @ProtoField
    String name;
}

public class OneofSegmentPoint extends OneofSegmentDomain {

    @ProtoConverter(converter = IntStringConverter.class)
    @ProtoField(protoName = "point")
    private String value;
}

@ProtoClassMapper(mapper = SegmentRangeMapper.class)
public class OneofSegmentRange extends OneofSegmentDomain {
    int start;
    int end;
}
```


#### Map oneof groups to a base class field
It is possible to place _@OneofBase_ and _@OneofField_ above a field in a class. In that case the field is mapped to a oneof group and the domain classes in _@OneofField_ implements or extends its type.

In this case the sub-classes behave as if they are mapped to a class which contains only the mentioned "protoField". _@ProtoField_ is used to map the domain field. In the absense of _ProtoField_, the field with a matching name is selected. Other annotations like @ProtoConstructor, @ProtoBuilder and @ProtoConverter may be used as well.

Special notes:
* It is possible to put @ProtoConverter above the implementing class as well. It will convert beween the protobuf field value and the entire class object. @ProtoClassMapper is not allowed since it has to map to a message.
* If the protobuf field is a message then it is possible to to have a regular _@ProtoClass_ annotation above the the sub-class.
* It is possible to have such mappings to several oneof groups in the same message.


##### Protobuf
```proto
message OneofSegmentFieldProto {
    string name = 1;
    oneof value {
        int32 point = 2;
        string range = 3;
    }
}
```

##### Domain
```java
@ProtoClass(protoClass = OneofSegmentFieldProto.class)
@Data
public class OneofSegmentFieldDomain {
    @ProtoField
    private String name;

    @OneofBase(oneofName = "value", oneOfFields = {
            @OneofField(protoField = "point", domainClass = OneofSegmentFieldPoint.class),
            @OneofField(protoField = "range", domainClass = OneofSegmentFieldRange.class)
    })
    private OneofSegmentField segment;
}

public interface OneofSegmentField {
}

public class OneofSegmentFieldPoint implements OneofSegmentField {

    @ProtoConverter(converter = IntStringConverter.class)
    @ProtoField(protoName = "point")
    private String value;
}

@ProtoConverter(converter = SegmentRangeConverter.class)
public class OneofSegmentFieldRange implements OneofSegmentField {
    int start;
    int end;
}
```

###### Protobuf
```protobuf
message ExampleProto {
    string string_value = 1;
    int32 int_value = 2;
}
```

### Multiple domain classes to a single protobuf message

Every `toDomain` function also has an overload that allows the user to specify the exact domain type.
This is required when multiple domain classes are mapped to the same protobuf message.

```java
@ProtoClass(protoClass = ExampleProto.class)
@Data
public class ExampleDomain {
    @ProtoField
    private String stringValue;
    @ProtoField
    private int intValue;
}

@ProtoClass(protoClass = ExampleProto.class)
@Data
public class ExampleDomain2 {
    @ProtoField
    private String stringValue;
    @ProtoField
    private int intValue;
}
```

In this example, the `toDomain` function will require a second argument:
```java
ProtoDomainConverter.toDomain(exampleProto, (ExampleDomain2) null);
```

This uses overload resolution to select the correct `toDomain` function at compile time.

#### Default domain class

If there is one domain class that should be used when calling `toDomain` with just one argument,
that domain class needs to be marked with `@ProtoClassDefault`.

```java
@ProtoClass(protoClass = ExampleProto.class)
@ProtoClassDefault
@Data
public class ExampleDomain {
    @ProtoField
    private String stringValue;
    @ProtoField
    private int intValue;
}

@ProtoClass(protoClass = ExampleProto.class)
@Data
public class ExampleDomain2 {
    @ProtoField
    private String stringValue;
    @ProtoField
    private int intValue;
}
```


## Roadmap

Please use [open issues](https://gitlab.com/protobuf-tools/proto_domain_converter/-/issues) to propose features and report defects.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

## License

Distributed under the APACHE 2 License. See [LICENSE](./LICENSE) for more information.

## Contact

Barak Silbert - silbert.barak@gmail.com 

Project Link: <https://gitlab.com/protobuf-tools/proto_domain_converter>
