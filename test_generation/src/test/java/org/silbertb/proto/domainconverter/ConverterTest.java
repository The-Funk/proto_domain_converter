package org.silbertb.proto.domainconverter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.silbertb.proto.domainconverter.domain.AllInOne;
import org.silbertb.proto.domainconverter.domain.AllInOneConstructor;
import org.silbertb.proto.domainconverter.domain.ConcreteMapToMessage;
import org.silbertb.proto.domainconverter.domain.ConcreteMessageList;
import org.silbertb.proto.domainconverter.domain.ConcretePrimitiveList;
import org.silbertb.proto.domainconverter.domain.ConcretePrimitiveMap;
import org.silbertb.proto.domainconverter.domain.MapToMessage;
import org.silbertb.proto.domainconverter.domain.OneofWithoutInheritance;
import org.silbertb.proto.domainconverter.domain.PrimitiveList;
import org.silbertb.proto.domainconverter.domain.PrimitiveMap;
import org.silbertb.proto.domainconverter.domain.SimpleContainer;
import org.silbertb.proto.domainconverter.domain.StringList;
import org.silbertb.proto.domainconverter.domain.*;
import org.silbertb.proto.domainconverter.domain.blacklist.BlacklistDomain;
import org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_class.BlacklistOneofBaseDomain;
import org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field.BlacklistOneofFieldDomain;
import org.silbertb.proto.domainconverter.domain.builder.*;
import org.silbertb.proto.domainconverter.domain.custom_converter.CustomConverter;
import org.silbertb.proto.domainconverter.domain.custom_converter.CustomListConverter;
import org.silbertb.proto.domainconverter.domain.custom_converter.CustomMapConverter;
import org.silbertb.proto.domainconverter.domain.custom_converter.MultiCustomConvertersDomain;
import org.silbertb.proto.domainconverter.domain.custom_mapper.CustomMapperTest;
import org.silbertb.proto.domainconverter.domain.multidomain.Library;
import org.silbertb.proto.domainconverter.domain.multidomain.OneofHolder;
import org.silbertb.proto.domainconverter.domain.multidomain.filesystem.FileNode;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean.OneofSegmentDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder.OneofSegmentBuilderDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor.OneofSegmentConstructorDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder.OneofSegmentConstructorBuilderDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.no_members.ShapeDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_field.OneofSegmentFieldDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_class.OneofImplHierarchyDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_field.OneofImplHierarchyFieldDomain;
import org.silbertb.proto.domainconverter.domain.oneof.field.OneofWithFieldInheritance;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.*;
import org.silbertb.proto.domainconverter.test.proto.multidomain.FileNodeMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.LibraryMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.OneofHolderMsg;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

public class ConverterTest {

    @Test
    void testPrimitivesToProto() {
        PrimitiveDomain domain = TestItemsCreator.createPrimitiveDomain();
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.Primitives expected = TestItemsCreator.createPrimitivesProto();

        assertEquals(expected, proto);
    }

    @Test
    void testPrimitivesToDomain() {
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = TestItemsCreator.createPrimitivesProto();
        PrimitiveDomain domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveDomain expected = TestItemsCreator.createPrimitiveDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyPrimitivesToProto() {
        PrimitiveDomain domain = new PrimitiveDomain();
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.Primitives expected =
                org.silbertb.proto.domainconverter.test.proto.Primitives.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyPrimitivesToDomain() {
        org.silbertb.proto.domainconverter.test.proto.Primitives proto =
                org.silbertb.proto.domainconverter.test.proto.Primitives.newBuilder().build();
        PrimitiveDomain domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveDomain expected = new PrimitiveDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testStringToProto() {
        StringDomain domain = TestItemsCreator.createStringDomain();
        StringProto proto = ProtoDomainConverter.toProto(domain);
        StringProto expected = TestItemsCreator.createStringProto();

        assertEquals(expected, proto);
    }

    @Test
    void testStringToDomain() {
        StringProto proto = TestItemsCreator.createStringProto();
        StringDomain domain = ProtoDomainConverter.toDomain(proto);
        StringDomain expected = TestItemsCreator.createStringDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyStringToProto() {
        StringDomain domain = new StringDomain();
        StringProto proto = ProtoDomainConverter.toProto(domain);
        StringProto expected = StringProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testBytesToProto() {
        BytesDomain domain = TestItemsCreator.createBytesDomain();
        BytesProto proto = ProtoDomainConverter.toProto(domain);
        BytesProto expected = TestItemsCreator.createBytesProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBytesToDomain() {
        BytesProto proto = TestItemsCreator.createBytesProto();
        BytesDomain domain = ProtoDomainConverter.toDomain(proto);
        BytesDomain expected = TestItemsCreator.createBytesDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyBytesToProto() {
        BytesDomain domain = new BytesDomain();
        BytesProto proto = ProtoDomainConverter.toProto(domain);
        BytesProto expected = BytesProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyStringToDomain() {
        StringProto proto = StringProto.newBuilder().build();
        StringDomain domain = ProtoDomainConverter.toDomain(proto);
        StringDomain expected = new StringDomain();
        expected.setStringValue("");

        assertEquals(expected, domain);
    }

    @Test
    void testSimpleContainerToProto() {
        SimpleContainer domain = TestItemsCreator.createSimpleContainerDomain();
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer expected = TestItemsCreator.createSimpleContainerProto();

        assertEquals(expected, proto);
    }

    @Test
    void testSimpleContainerToDomain() {
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto = TestItemsCreator.createSimpleContainerProto();
        SimpleContainer domain = ProtoDomainConverter.toDomain(proto);
        SimpleContainer expected = TestItemsCreator.createSimpleContainerDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptySimpleContainerToProto() {
        SimpleContainer domain = new SimpleContainer();
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer expected =
                org.silbertb.proto.domainconverter.test.proto.SimpleContainer.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptySimpleContainerToDomain() {
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto =
                org.silbertb.proto.domainconverter.test.proto.SimpleContainer.newBuilder().build();
        SimpleContainer domain = ProtoDomainConverter.toDomain(proto);
        SimpleContainer expected = new SimpleContainer();

        assertEquals(expected, domain);
    }

    @Test
    void testPrimitiveListToProto() {
        PrimitiveList domain = TestItemsCreator.createPrimitiveListDomain();
        org.silbertb.proto.domainconverter.test.proto.PrimitiveList proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.PrimitiveList expected = TestItemsCreator.createPrimitiveListProto();

        assertEquals(expected, proto);
    }

    @Test
    void testPrimitiveListToDomain() {
        org.silbertb.proto.domainconverter.test.proto.PrimitiveList proto = TestItemsCreator.createPrimitiveListProto();
        PrimitiveList domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveList expected = TestItemsCreator.createPrimitiveListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyPrimitiveListToProto() {
        PrimitiveList domain = new PrimitiveList();
        org.silbertb.proto.domainconverter.test.proto.PrimitiveList proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.PrimitiveList expected =
                org.silbertb.proto.domainconverter.test.proto.PrimitiveList.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyPrimitiveListToDomain() {
        org.silbertb.proto.domainconverter.test.proto.PrimitiveList proto =
                org.silbertb.proto.domainconverter.test.proto.PrimitiveList.newBuilder().build();
        PrimitiveList domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveList expected = new PrimitiveList();
        expected.setIntList(Collections.emptyList());

        assertEquals(expected, domain);
    }

    @Test
    void testPrimitiveConcreteListToDomain() {
        org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveList proto = TestItemsCreator.createConcretePrimitiveListProto();
        ConcretePrimitiveList domain = ProtoDomainConverter.toDomain(proto);
        ConcretePrimitiveList expected = TestItemsCreator.createConcretePrimitiveListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testStringListToProto() {
        StringList domain = TestItemsCreator.createStringListDomain();
        org.silbertb.proto.domainconverter.test.proto.StringList proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.StringList expected = TestItemsCreator.createStringListProto();

        assertEquals(expected, proto);
    }

    @Test
    void testStringListToDomain() {
        org.silbertb.proto.domainconverter.test.proto.StringList proto = TestItemsCreator.createStringListProto();
        StringList domain = ProtoDomainConverter.toDomain(proto);
        StringList expected = TestItemsCreator.createStringListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMessageListToProto() {
        MessageListDomain domain = TestItemsCreator.createMessageListDomain();
        org.silbertb.proto.domainconverter.test.proto.MessageList proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.MessageList expected = TestItemsCreator.createMessageListProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMessageListToDomain() {
        org.silbertb.proto.domainconverter.test.proto.MessageList proto = TestItemsCreator.createMessageListProto();
        MessageListDomain domain = ProtoDomainConverter.toDomain(proto);
        MessageListDomain expected = TestItemsCreator.createMessageListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyMessageListToProto() {
        MessageListDomain domain = new MessageListDomain();
        org.silbertb.proto.domainconverter.test.proto.MessageList proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.MessageList expected =
                org.silbertb.proto.domainconverter.test.proto.MessageList.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testConcreteMessageListToDomain() {
        org.silbertb.proto.domainconverter.test.proto.ConcreteMessageList proto = TestItemsCreator.createConcreteMessageListProto();
        ConcreteMessageList domain = ProtoDomainConverter.toDomain(proto);
        ConcreteMessageList expected = TestItemsCreator.createConcreteMessageListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testPrimitiveMapToProto() {
        PrimitiveMap domain = TestItemsCreator.createPrimitiveMapDomain();
        org.silbertb.proto.domainconverter.test.proto.PrimitiveMap proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.PrimitiveMap expected = TestItemsCreator.createPrimitiveMapProto();

        assertEquals(expected, proto);
    }

    @Test
    void testPrimitiveMapToDomain() {
        org.silbertb.proto.domainconverter.test.proto.PrimitiveMap proto = TestItemsCreator.createPrimitiveMapProto();
        PrimitiveMap domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveMap expected = TestItemsCreator.createPrimitiveMapDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyPrimitiveMapToProto() {
        PrimitiveMap domain = new PrimitiveMap();
        org.silbertb.proto.domainconverter.test.proto.PrimitiveMap proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.PrimitiveMap expected =
                org.silbertb.proto.domainconverter.test.proto.PrimitiveMap.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyPrimitiveMapToDomain() {
        org.silbertb.proto.domainconverter.test.proto.PrimitiveMap proto =
                org.silbertb.proto.domainconverter.test.proto.PrimitiveMap.newBuilder().build();
        PrimitiveMap domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveMap expected = new PrimitiveMap();
        expected.setPrimitiveMap(Collections.emptyMap());

        assertEquals(expected, domain);
    }

    @Test
    void testConcretePrimitiveMapToDomain() {
        org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveMap proto = TestItemsCreator.createConcretePrimitiveMapProto();
        ConcretePrimitiveMap domain = ProtoDomainConverter.toDomain(proto);
        ConcretePrimitiveMap expected = TestItemsCreator.createConcretePrimitiveMapDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMapToMessageToProto() {
        MapToMessage domain = TestItemsCreator.createMapToMessageDomain();
        org.silbertb.proto.domainconverter.test.proto.MapToMessage proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.MapToMessage expected = TestItemsCreator.createMapToMessageProto();

        assertEquals(expected, proto);
    }

    @Test
    void testConcreteMapToMessageToDomain() {
        org.silbertb.proto.domainconverter.test.proto.ConcreteMapToMessage proto = TestItemsCreator.createConcreteMapToMessageProto();
        ConcreteMapToMessage domain = ProtoDomainConverter.toDomain(proto);
        ConcreteMapToMessage expected = TestItemsCreator.createConcreteMapToMessageDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofWithoutInheritanceToProto() {
        OneofWithoutInheritance domain = TestItemsCreator.createOneofWithoutInheritanceDomain();
        org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance expected = TestItemsCreator.createOneofWithoutInheritanceProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofWithoutInheritanceToDomain() {
        org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance proto = TestItemsCreator.createOneofWithoutInheritanceProto();
        OneofWithoutInheritance domain = ProtoDomainConverter.toDomain(proto);
        OneofWithoutInheritance expected = TestItemsCreator.createOneofWithoutInheritanceDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofWithInheritanceToProto() {
        OneofWithFieldInheritance domain = TestItemsCreator.createOneofWithInheritanceDomain();
        org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance expected = TestItemsCreator.createOneofWithFieldInheritanceProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofWithInheritanceToDomain() {
        org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance proto = TestItemsCreator.createOneofWithFieldInheritanceProto();
        OneofWithFieldInheritance domain = ProtoDomainConverter.toDomain(proto);
        OneofWithFieldInheritance expected = TestItemsCreator.createOneofWithInheritanceDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testAllInOneToProto() {
        AllInOne domain = TestItemsCreator.createAllInOneDomain();
        org.silbertb.proto.domainconverter.test.proto.AllInOne proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.AllInOne expected = TestItemsCreator.createAllInOneProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneToDomain() {
        org.silbertb.proto.domainconverter.test.proto.AllInOne proto = TestItemsCreator.createAllInOneProto();
        AllInOne domain = ProtoDomainConverter.toDomain(proto);
        AllInOne expected = TestItemsCreator.createAllInOneDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomConverterToDomain() {
        org.silbertb.proto.domainconverter.test.proto.CustomConverter proto = TestItemsCreator.createCustomConverterProto();
        CustomConverter domain = ProtoDomainConverter.toDomain(proto);
        CustomConverter expected = TestItemsCreator.createCustomConverterDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomConverterToProto() {
        CustomConverter domain = TestItemsCreator.createCustomConverterDomain();
        org.silbertb.proto.domainconverter.test.proto.CustomConverter proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.CustomConverter expected = TestItemsCreator.createCustomConverterProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomListConverterToDomain() {
        org.silbertb.proto.domainconverter.test.proto.CustomListConverter proto = TestItemsCreator.createCustomListConverterProto();
        CustomListConverter domain = ProtoDomainConverter.toDomain(proto);
        CustomListConverter expected = TestItemsCreator.createCustomListConverterDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomListConverterToProto() {
        CustomListConverter domain = TestItemsCreator.createCustomListConverterDomain();
        org.silbertb.proto.domainconverter.test.proto.CustomListConverter proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.CustomListConverter expected = TestItemsCreator.createCustomListConverterProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomMapConverterToDomain() {
        org.silbertb.proto.domainconverter.test.proto.CustomMapConverter proto = TestItemsCreator.createCustomMapConverterProto();
        CustomMapConverter domain = ProtoDomainConverter.toDomain(proto);
        CustomMapConverter expected = TestItemsCreator.createCustomMapConverterDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomMapConverterToProto() {
        CustomMapConverter domain = TestItemsCreator.createCustomMapConverterDomain();
        org.silbertb.proto.domainconverter.test.proto.CustomMapConverter proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.CustomMapConverter expected = TestItemsCreator.createCustomMapConverterProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMultiCustomConvertersToDomain() {
        MultiCustomConvertersProto proto = TestItemsCreator.createMultiCustomConvertersProto();
        MultiCustomConvertersDomain domain = ProtoDomainConverter.toDomain(proto);
        MultiCustomConvertersDomain expected = TestItemsCreator.createMultiCustomConvertersDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMultiCustomConvertersToProto() {
        MultiCustomConvertersDomain domain = TestItemsCreator.createMultiCustomConvertersDomain();
        MultiCustomConvertersProto proto = ProtoDomainConverter.toProto(domain);
        MultiCustomConvertersProto expected = TestItemsCreator.createMultiCustomConvertersProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomMapperToDomain() {
        org.silbertb.proto.domainconverter.test.proto.CustomMapperTest proto = TestItemsCreator.createCustomMapperProto();
        CustomMapperTest domain = ProtoDomainConverter.toDomain(proto);
        CustomMapperTest expected = TestItemsCreator.createCustomMapperDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomMapperToProto() {
        CustomMapperTest domain = TestItemsCreator.createCustomMapperDomain();
        org.silbertb.proto.domainconverter.test.proto.CustomMapperTest proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.CustomMapperTest expected = TestItemsCreator.createCustomMapperProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneConstructorToProto() {
        AllInOneConstructor domain = TestItemsCreator.createAllInOneConstructorDomain();
        org.silbertb.proto.domainconverter.test.proto.AllInOneConstructor proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.AllInOneConstructor expected = TestItemsCreator.createAllInOneConstructorProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneConstructorToDomain() {
        org.silbertb.proto.domainconverter.test.proto.AllInOneConstructor proto = TestItemsCreator.createAllInOneConstructorProto();
        AllInOneConstructor domain = ProtoDomainConverter.toDomain(proto);
        AllInOneConstructor expected = TestItemsCreator.createAllInOneConstructorDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testAllInOneBuilderToProto() {
        AllInOneBuilderDomain domain = TestItemsCreator.createAllInOneBuilderDomain();
        AllInOneBuilderProto proto = ProtoDomainConverter.toProto(domain);
        AllInOneBuilderProto expected = TestItemsCreator.createAllInOneBuilderProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneBuilderToDomain() {
        AllInOneBuilderProto proto = TestItemsCreator.createAllInOneBuilderProto();
        AllInOneBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        AllInOneBuilderDomain expected = TestItemsCreator.createAllInOneBuilderDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomBuilderMethodToProto() {
        CustomBuilderMethodDomain domain = TestItemsCreator.createCustomerBuilderMethodDomain();
        CustomBuilderMethodProto proto = ProtoDomainConverter.toProto(domain);
        CustomBuilderMethodProto expected = TestItemsCreator.createCustomBuilderMethodProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomBuilderMethodToDomain() {
        CustomBuilderMethodProto proto = TestItemsCreator.createCustomBuilderMethodProto();
        CustomBuilderMethodDomain domain = ProtoDomainConverter.toDomain(proto);
        CustomBuilderMethodDomain expected = TestItemsCreator.createCustomerBuilderMethodDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomBuildMethodToProto() {
        CustomBuildMethodDomain domain = TestItemsCreator.createCustomerBuildMethodDomain();
        CustomBuildMethodProto proto = ProtoDomainConverter.toProto(domain);
        CustomBuildMethodProto expected = TestItemsCreator.createCustomBuildMethodProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomBuildMethodToDomain() {
        CustomBuildMethodProto proto = TestItemsCreator.createCustomBuildMethodProto();
        CustomBuildMethodDomain domain = ProtoDomainConverter.toDomain(proto);
        CustomBuildMethodDomain expected = TestItemsCreator.createCustomerBuildMethodDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomSetterPrefixToProto() {
        BuilderCustomSetterPrefixDomain domain = TestItemsCreator.createBuilderCustomSetterPrefixDomain();
        BuilderCustomSetterPrefixProto proto = ProtoDomainConverter.toProto(domain);
        BuilderCustomSetterPrefixProto expected = TestItemsCreator.createBuilderCustomSetterPrefixProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomSetterPrefixToDomain() {
        BuilderCustomSetterPrefixProto proto = TestItemsCreator.createBuilderCustomSetterPrefixProto();
        BuilderCustomSetterPrefixDomain domain = ProtoDomainConverter.toDomain(proto);
        BuilderCustomSetterPrefixDomain expected = TestItemsCreator.createBuilderCustomSetterPrefixDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testAllInOneBuilderConstructorToProto() {
        AllInOneBuilderConstructorDomain domain = TestItemsCreator.createAllInOneBuilderConstructorDomain();
        AllInOneBuilderConstructorProto proto = ProtoDomainConverter.toProto(domain);
        AllInOneBuilderConstructorProto expected = TestItemsCreator.createAllInOneBuilderConstructorProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneBuilderToConstructorDomain() {
        AllInOneBuilderConstructorProto proto = TestItemsCreator.createAllInOneBuilderConstructorProto();
        AllInOneBuilderConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        AllInOneBuilderConstructorDomain expected = TestItemsCreator.createAllInOneBuilderConstructorDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBeanPrimitiveToProto() {
        OneofSegmentDomain domain = TestItemsCreator.createOneofSegmentPointDomain();
        OneofSegmentProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentProto expected = TestItemsCreator.createOneofSegmentPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBeanPrimitiveToDomain() {
        OneofSegmentProto proto = TestItemsCreator.createOneofSegmentPointProto();
        OneofSegmentDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentDomain expected = TestItemsCreator.createOneofSegmentPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBeanMessageToProto() {
        OneofSegmentDomain domain = TestItemsCreator.createOneofSegmentOpenRangeDomain();
        OneofSegmentProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentProto expected = TestItemsCreator.createOneofSegmentOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBeanMessageToDomain() {
        OneofSegmentProto proto = TestItemsCreator.createOneofSegmentOpenRangeProto();
        OneofSegmentDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentDomain expected = TestItemsCreator.createOneofSegmentOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBeanMapperToProto() {
        OneofSegmentDomain domain = TestItemsCreator.createOneofSegmentRangeDomain();
        OneofSegmentProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentProto expected = TestItemsCreator.createOneofSegmentRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBeanMapperToDomain() {
        OneofSegmentProto proto = TestItemsCreator.createOneofSegmentRangeProto();
        OneofSegmentDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentDomain expected = TestItemsCreator.createOneofSegmentRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBuilderPrimitiveToProto() {
        OneofSegmentBuilderDomain domain = TestItemsCreator.createOneofSegmentBuilderPointDomain();
        OneofSegmentBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentBuilderProto expected = TestItemsCreator.createOneofSegmentBuilderPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBuilderPrimitiveToDomain() {
        OneofSegmentBuilderProto proto = TestItemsCreator.createOneofSegmentBuilderPointProto();
        OneofSegmentBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentBuilderDomain expected = TestItemsCreator.createOneofSegmentBuilderPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBuilderMessageToProto() {
        OneofSegmentBuilderDomain domain = TestItemsCreator.createOneofSegmentBuilderOpenRangeDomain();
        OneofSegmentBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentBuilderProto expected = TestItemsCreator.createOneofSegmentBuilderOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBuilderMessageToDomain() {
        OneofSegmentBuilderProto proto = TestItemsCreator.createOneofSegmentBuilderOpenRangeProto();
        OneofSegmentBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentBuilderDomain expected = TestItemsCreator.createOneofSegmentBuilderOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBuilderMapperToProto() {
        OneofSegmentBuilderDomain domain = TestItemsCreator.createOneofSegmentBuilderRangeDomain();
        OneofSegmentBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentBuilderProto expected = TestItemsCreator.createOneofSegmentBuilderRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBuilderMapperToDomain() {
        OneofSegmentBuilderProto proto = TestItemsCreator.createOneofSegmentBuilderRangeProto();
        OneofSegmentBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentBuilderDomain expected = TestItemsCreator.createOneofSegmentBuilderRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorPrimitiveToProto() {
        OneofSegmentConstructorDomain domain = TestItemsCreator.createOneofSegmentConstructorPointDomain();
        OneofSegmentConstructorProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorProto expected = TestItemsCreator.createOneofSegmentConstructorPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorPrimitiveToDomain() {
        OneofSegmentConstructorProto proto = TestItemsCreator.createOneofSegmentConstructorPointProto();
        OneofSegmentConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorDomain expected = TestItemsCreator.createOneofSegmentConstructorPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorMessageToProto() {
        OneofSegmentConstructorDomain domain = TestItemsCreator.createOneofSegmentConstructorOpenRangeDomain();
        OneofSegmentConstructorProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorProto expected = TestItemsCreator.createOneofSegmentConstructorOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorMessageToDomain() {
        OneofSegmentConstructorProto proto = TestItemsCreator.createOneofSegmentConstructorOpenRangeProto();
        OneofSegmentConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorDomain expected = TestItemsCreator.createOneofSegmentConstructorOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorMapperToProto() {
        OneofSegmentConstructorDomain domain = TestItemsCreator.createOneofSegmentConstructorRangeDomain();
        OneofSegmentConstructorProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorProto expected = TestItemsCreator.createOneofSegmentConstructorRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorMapperToDomain() {
        OneofSegmentConstructorProto proto = TestItemsCreator.createOneofSegmentConstructorRangeProto();
        OneofSegmentConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorDomain expected = TestItemsCreator.createOneofSegmentConstructorRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorBuilderPrimitiveToProto() {
        OneofSegmentConstructorBuilderDomain domain = TestItemsCreator.createOneofSegmentConstructorBuilderPointDomain();
        OneofSegmentConstructorBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorBuilderProto expected = TestItemsCreator.createOneofSegmentConstructorBuilderPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorBuilderPrimitiveToDomain() {
        OneofSegmentConstructorBuilderProto proto = TestItemsCreator.createOneofSegmentConstructorBuilderPointProto();
        OneofSegmentConstructorBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorBuilderDomain expected = TestItemsCreator.createOneofSegmentConstructorBuilderPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorBuilderMessageToProto() {
        OneofSegmentConstructorBuilderDomain domain = TestItemsCreator.createOneofSegmentConstructorBuilderOpenRangeDomain();
        OneofSegmentConstructorBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorBuilderProto expected = TestItemsCreator.createOneofSegmentConstructorBuilderOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorBuilderMessageToDomain() {
        OneofSegmentConstructorBuilderProto proto = TestItemsCreator.createOneofSegmentConstructorBuilderOpenRangeProto();
        OneofSegmentConstructorBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorBuilderDomain expected = TestItemsCreator.createOneofSegmentConstructorBuilderOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorBuilderMapperToProto() {
        OneofSegmentConstructorBuilderDomain domain = TestItemsCreator.createOneofSegmentConstructorBuilderRangeDomain();
        OneofSegmentConstructorBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorBuilderProto expected = TestItemsCreator.createOneofSegmentConstructorBuilderRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorBuilderMapperToDomain() {
        OneofSegmentConstructorBuilderProto proto = TestItemsCreator.createOneofSegmentConstructorBuilderRangeProto();
        OneofSegmentConstructorBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorBuilderDomain expected = TestItemsCreator.createOneofSegmentConstructorBuilderRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassMessageToProto() {
        ShapeDomain domain = TestItemsCreator.createShapeDomain();
        ShapeProto proto = ProtoDomainConverter.toProto(domain);
        ShapeProto expected = TestItemsCreator.createShapeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassMessageToDomain() {
        ShapeProto proto = TestItemsCreator.createShapeProto();
        ShapeDomain domain = ProtoDomainConverter.toDomain(proto);
        ShapeDomain expected = TestItemsCreator.createShapeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldBeanPrimitiveToDomain() {
        OneofSegmentFieldProto proto = TestItemsCreator.createOneofSegmentFieldPointProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = TestItemsCreator.createOneofSegmentFieldPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldBeanPrimitiveToProto() {
        OneofSegmentFieldDomain domain = TestItemsCreator.createOneofSegmentFieldPointDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = TestItemsCreator.createOneofSegmentFieldPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldConverterToDomain() {
        OneofSegmentFieldProto proto = TestItemsCreator.createOneofSegmentFieldRangeProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = TestItemsCreator.createOneofSegmentFieldRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldConverterToProto() {
        OneofSegmentFieldDomain domain = TestItemsCreator.createOneofSegmentFieldRangeDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = TestItemsCreator.createOneofSegmentFieldRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldConstructorToDomain() {
        OneofSegmentFieldProto proto = TestItemsCreator.createOneofSegmentFieldInfinityStartProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = TestItemsCreator.createOneofSegmentFieldInfinityStartDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldConstructorToProto() {
        OneofSegmentFieldDomain domain = TestItemsCreator.createOneofSegmentFieldInfinityStartDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = TestItemsCreator.createOneofSegmentFieldInfinityStartProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldMessageToDomain() {
        OneofSegmentFieldProto proto = TestItemsCreator.createOneofSegmentFieldEmptyRangeProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = TestItemsCreator.createOneofSegmentFieldEmptyRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldMessageToProto() {
        OneofSegmentFieldDomain domain = TestItemsCreator.createOneofSegmentFieldEmptyRangeDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = TestItemsCreator.createOneofSegmentFieldEmptyRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldConstructorBuilderToDomain() {
        OneofSegmentFieldProto proto = TestItemsCreator.createOneofSegmentFieldInfinityEndProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = TestItemsCreator.createOneofSegmentFieldInfinityEndDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldConstructorBuilderToProto() {
        OneofSegmentFieldDomain domain = TestItemsCreator.createOneofSegmentFieldInfinityEndDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = TestItemsCreator.createOneofSegmentFieldInfinityEndProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldBuilderToDomain() {
        OneofSegmentFieldProto proto = TestItemsCreator.createOneofSegmentFieldOpenRangeProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = TestItemsCreator.createOneofSegmentFieldOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldBuilderToProto() {
        OneofSegmentFieldDomain domain = TestItemsCreator.createOneofSegmentFieldOpenRangeDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = TestItemsCreator.createOneofSegmentFieldOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchyToDomain() {
        OneofImplHierarchyProto proto = TestItemsCreator.createOneofImplHierarchyProto();
        OneofImplHierarchyDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofImplHierarchyDomain expected = TestItemsCreator.createOneofImplHierarchyDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofDomainImplHierarchyToProto() {
        OneofImplHierarchyDomain domain = TestItemsCreator.createOneofImplHierarchyDomain();
        OneofImplHierarchyProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyProto expected = TestItemsCreator.createOneofImplHierarchyProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchySubclassToProto() {
        OneofImplHierarchyDomain domain = TestItemsCreator.createOneofImplHierarchyDomainSubclass();
        OneofImplHierarchyProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyProto expected = TestItemsCreator.createOneofImplHierarchyProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchyFieldToDomain() {
        OneofImplHierarchyFieldProto proto = TestItemsCreator.createOneofImplHierarchyFieldProto();
        OneofImplHierarchyFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofImplHierarchyFieldDomain expected = TestItemsCreator.createOneofImplHierarchyFieldDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofDomainImplHierarchyFieldToProto() {
        OneofImplHierarchyFieldDomain domain = TestItemsCreator.createOneofImplHierarchyFieldDomain();
        OneofImplHierarchyFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyFieldProto expected = TestItemsCreator.createOneofImplHierarchyFieldProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchyFieldSubclassToProto() {
        OneofImplHierarchyFieldDomain domain = TestItemsCreator.createOneofImplHierarchyFieldDomainSubclass();
        OneofImplHierarchyFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyFieldProto expected = TestItemsCreator.createOneofImplHierarchyFieldProto();

        assertEquals(expected, proto);
    }


    @Test
    void testBlacklistToDomain() {
        BlacklistProto proto = TestItemsCreator.createBlacklistProto();
        BlacklistDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistDomain expected = TestItemsCreator.createBlacklistDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistToProto() {
        BlacklistDomain domain = TestItemsCreator.createBlacklistDomain();
        BlacklistProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistProto expected = TestItemsCreator.createBlacklistProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofStringToDomain() {
        BlacklistOneofFieldProto proto = TestItemsCreator.createBlacklistOneofFieldStringProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = TestItemsCreator.createBlacklistOneofFieldStringDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofStringToProto() {
        BlacklistOneofFieldDomain domain = TestItemsCreator.createBlacklistOneofFieldStringDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = TestItemsCreator.createBlacklistOneofFieldStringProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofBooleanToDomain() {
        BlacklistOneofFieldProto proto = TestItemsCreator.createBlacklistOneofFieldBoolProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = TestItemsCreator.createBlacklistOneofFieldBoolDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofBooleanToProto() {
        BlacklistOneofFieldDomain domain = TestItemsCreator.createBlacklistOneofFieldBoolDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = TestItemsCreator.createBlacklistOneofFieldBoolProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofBlacklistMsgToDomain() {
        BlacklistOneofFieldProto proto = TestItemsCreator.createBlacklistOneofFieldBlacklistMsgProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = TestItemsCreator.createBlacklistOneofFieldBlacklistMsgDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofBlacklistMsgToProto() {
        BlacklistOneofFieldDomain domain = TestItemsCreator.createBlacklistOneofFieldBlacklistMsgDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = TestItemsCreator.createBlacklistOneofFieldBlacklistMsgProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofWhitelistMsgToDomain() {
        BlacklistOneofFieldProto proto = TestItemsCreator.createBlacklistOneofFieldWhitelistMsgProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = TestItemsCreator.createBlacklistOneofFieldWhitelistMsgDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofWhitelistMsgToProto() {
        BlacklistOneofFieldDomain domain = TestItemsCreator.createBlacklistOneofFieldWhitelistMsgDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = TestItemsCreator.createBlacklistOneofFieldWhitelistMsgProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofInnerMsgToDomain() {
        BlacklistOneofFieldProto proto = TestItemsCreator.createBlacklistOneofFieldInnerMsgProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = TestItemsCreator.createBlacklistOneofFieldInnerMsgDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofInnerMsgToProto() {
        BlacklistOneofFieldDomain domain = TestItemsCreator.createBlacklistOneofFieldInnerMsgDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = TestItemsCreator.createBlacklistOneofFieldInnerMsgProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofBaseToDomain() {
        BlacklistOneofBaseProto proto = TestItemsCreator.createBlacklistOneofBaseProto();
        BlacklistOneofBaseDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofBaseDomain expected = TestItemsCreator.createBlacklistOneofBaseDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofBaseProto() {
        BlacklistOneofBaseDomain domain = TestItemsCreator.createBlacklistOneofBaseDomain();
        BlacklistOneofBaseProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofBaseProto expected = TestItemsCreator.createBlacklistOneofBaseProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMultidomainToDomain() {
        LibraryMsg proto = TestItemsCreator.createLibraryProto();
        Library domain = ProtoDomainConverter.toDomain(proto);
        Library expected = TestItemsCreator.createLibraryDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMultidomainToProto() {
        Library domain = TestItemsCreator.createLibraryDomain();
        LibraryMsg proto = ProtoDomainConverter.toProto(domain);
        LibraryMsg expected = TestItemsCreator.createLibraryProto();

        assertEquals(expected, proto);
    }

    @ParameterizedTest(name = "{index} => valueCase=''{0}''")
    @EnumSource(OneofHolderMsg.ValueCase.class)
    void testMultidomainOneofFieldToDomain(OneofHolderMsg.ValueCase valueCase) {
        assumeFalse(valueCase == OneofHolderMsg.ValueCase.VALUE_NOT_SET);
        OneofHolderMsg proto = TestItemsCreator.createOneofHolderProto(valueCase);
        OneofHolder domain = ProtoDomainConverter.toDomain(proto);
        OneofHolder expected = TestItemsCreator.createOneofHolderDomain(valueCase);

        assertEquals(expected, domain);
    }

    @ParameterizedTest(name = "{index} => valueCase=''{0}''")
    @EnumSource(OneofHolderMsg.ValueCase.class)
    void testMultidomainOneofFieldToProto(OneofHolderMsg.ValueCase valueCase) {
        assumeFalse(valueCase == OneofHolderMsg.ValueCase.VALUE_NOT_SET);
        OneofHolder domain = TestItemsCreator.createOneofHolderDomain(valueCase);
        OneofHolderMsg proto = ProtoDomainConverter.toProto(domain);
        OneofHolderMsg expected = TestItemsCreator.createOneofHolderProto(valueCase);

        assertEquals(expected, proto);
    }

    @Test
    void testMultidomainOneofBaseToDomain() {
        FileNodeMsg proto = TestItemsCreator.createOneofBaseProto();
        FileNode domain = ProtoDomainConverter.toDomain(proto);
        FileNode expected = TestItemsCreator.createOneofBaseDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMultidomainOneofBaseToProto() {
        FileNode domain = TestItemsCreator.createOneofBaseDomain();
        FileNodeMsg proto = ProtoDomainConverter.toProto(domain);
        FileNodeMsg expected = TestItemsCreator.createOneofBaseProto();

        assertEquals(expected, proto);
    }
}
