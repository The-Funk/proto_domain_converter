package org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentProto;

@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "point", domainClass = OneofSegmentPoint.class, domainField = "value"),
        @OneofField(protoField = "open_range", domainClass = OneofSegmentOpenRange.class, domainField = "range"),
        @OneofField(protoField = "range", domainClass = OneofSegmentRange.class)
})
@ProtoClass(protoClass = OneofSegmentProto.class)
@Data
public class OneofSegmentDomain {
    @ProtoField
    String name;
}
