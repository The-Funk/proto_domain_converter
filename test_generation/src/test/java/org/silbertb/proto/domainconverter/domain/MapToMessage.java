package org.silbertb.proto.domainconverter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;

import java.util.Map;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.MapToMessage.class)
public class MapToMessage {

    @ProtoField
    Map<String, PrimitiveDomain> mapToMessage;
}
