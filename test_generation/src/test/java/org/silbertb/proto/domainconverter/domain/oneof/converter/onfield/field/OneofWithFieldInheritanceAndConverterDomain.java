package org.silbertb.proto.domainconverter.domain.oneof.converter.onfield.field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritanceAndConverterProto;

@Data
@ProtoClass(protoClass = OneofWithFieldInheritanceAndConverterProto.class)
public class OneofWithFieldInheritanceAndConverterDomain {
    @OneofBase(oneOfFields = {
            @OneofField(protoField = "int_val", domainClass = OneofBigIntFieldImpl.class, domainField = "bigIntVal"),
    })
    private OneofBaseFieldWithConverter value;
}
