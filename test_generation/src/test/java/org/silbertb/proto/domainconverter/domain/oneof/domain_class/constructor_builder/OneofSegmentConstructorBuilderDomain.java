package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentConstructorBuilderProto;

@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "point", domainClass = OneofSegmentConstructorBuilderPoint.class, domainField = "value"),
        @OneofField(protoField = "open_range", domainClass = OneofSegmentConstructorBuilderOpenRange.class, domainField = "range"),
        @OneofField(protoField = "range", domainClass = OneofSegmentConstructorBuilderRange.class)
})
@ProtoClass(protoClass = OneofSegmentConstructorBuilderProto.class)
@Data
public class OneofSegmentConstructorBuilderDomain {
    private final String name;
}
