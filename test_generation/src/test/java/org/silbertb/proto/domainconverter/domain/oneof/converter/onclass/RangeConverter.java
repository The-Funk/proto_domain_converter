package org.silbertb.proto.domainconverter.domain.oneof.converter.onclass;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

public class RangeConverter implements TypeConverter<Range, String> {

    @Override
    public Range toDomainValue(String protoValue) {
        String[] edges = protoValue.split("-");
        int start = Integer.parseInt(edges[0]);
        int end = Integer.parseInt(edges[1]);

        return new Range(start, end);
    }

    @Override
    public boolean shouldAssignToProto(Range range) {
        return true;
    }

    @Override
    public String toProtobufValue(Range range) {
        return range.getStart() + "-" + range.getEnd();
    }
}
