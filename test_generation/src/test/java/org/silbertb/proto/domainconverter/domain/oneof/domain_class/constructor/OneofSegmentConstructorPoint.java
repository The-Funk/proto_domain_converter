package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.IntStringConverter;

@EqualsAndHashCode(callSuper = true)
@Getter
public class OneofSegmentConstructorPoint extends OneofSegmentConstructorDomain {

    private final String value;

    @ProtoConstructor
    public OneofSegmentConstructorPoint(
            String name,
            @ProtoConverter(converter = IntStringConverter.class)
            @ProtoField(protoName = "point")
            String value) {
        super(name);
        this.value = value;
    }
}
