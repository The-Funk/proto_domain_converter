package org.silbertb.proto.domainconverter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;

import java.util.HashMap;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.ConcreteMapToMessage.class)
public class ConcreteMapToMessage {
    @ProtoField
    HashMap<String, PrimitiveDomain> mapToMessage;
}
