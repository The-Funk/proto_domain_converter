package org.silbertb.proto.domainconverter.domain.custom_mapper;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;
import org.silbertb.proto.domainconverter.domain.StringDomain;
import lombok.Data;

@Data
@ProtoClassMapper(mapper = CustomMapper.class)
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.class)
public class CustomMapperTest {
    private boolean isTcp;
    private boolean isUdp;
    private StringDomain stringDomain;

    public void setUdp(boolean udp) {
        isUdp = udp;
        isTcp = !udp;
    }

    public void setTcp(boolean tcp) {
        isTcp = tcp;
        isUdp = !tcp;
    }
}
