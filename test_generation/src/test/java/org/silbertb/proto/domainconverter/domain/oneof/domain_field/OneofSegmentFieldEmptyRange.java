package org.silbertb.proto.domainconverter.domain.oneof.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.EmptyRangeProto;

@ProtoClass(protoClass = EmptyRangeProto.class)
@Data
public class OneofSegmentFieldEmptyRange implements OneofSegmentField{
}
