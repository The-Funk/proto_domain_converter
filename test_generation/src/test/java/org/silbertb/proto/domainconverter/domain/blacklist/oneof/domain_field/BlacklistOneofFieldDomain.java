package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.BlacklistOneofFieldProto;

@Data
@ProtoClass(protoClass = BlacklistOneofFieldProto.class)
public class BlacklistOneofFieldDomain {
    @OneofBase(oneOfFields = {
            @OneofField(protoField = "string_value", domainClass = BlacklistOneofStringImpl.class),
            @OneofField(protoField = "boolean_value", domainClass = BlacklistOneofBoolImpl.class),
            @OneofField(protoField = "blacklist_msg", domainClass = BlacklistOneofImplDomain.class),
            @OneofField(protoField = "whitelist_msg", domainClass = WhitelistOneofImplDomain.class),
            @OneofField(protoField = "inner_msg", domainClass = InnerMsgOneofImplDomain.class),
    })
    private BlacklistOneof blacklistOneof;
}
