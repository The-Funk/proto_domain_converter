package org.silbertb.proto.domainconverter.domain.oneof.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.IntStringConverter;

@Data
public class OneofSegmentFieldPoint implements OneofSegmentField {

    @ProtoConverter(converter = IntStringConverter.class)
    @ProtoField(protoName = "point")
    private String value;
}
