package org.silbertb.proto.domainconverter.domain.oneof.domain_class.no_members;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.CircleProto;

@ProtoClass(protoClass = CircleProto.class)
@EqualsAndHashCode
@Getter
public class CircleDomain implements ShapeDomain {
    private final double radius;

    @ProtoConstructor
    public CircleDomain(@ProtoField double radius) {
        this.radius = radius;
    }

}
