package org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_class;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class OneofImplHierarchyImpl1 implements OneofImplHierarchyDomain {

    private final String val;

    @ProtoConstructor
    public OneofImplHierarchyImpl1(@ProtoField(protoName = "val1") String val) {
        this.val = val;
    }
}
