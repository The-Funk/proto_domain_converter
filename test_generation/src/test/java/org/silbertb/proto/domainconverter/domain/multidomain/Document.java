package org.silbertb.proto.domainconverter.domain.multidomain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

import java.util.List;

@Data
@EqualsAndHashCode
public abstract class Document {
    @ProtoField
    private String id;

    @ProtoField
    private String title;

    @ProtoField
    private List<Section> sections;
}
