package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_class;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BlacklistOneofDoubleImplDomain extends BlacklistOneofBaseDomain {
    private double doubleValue;
}
