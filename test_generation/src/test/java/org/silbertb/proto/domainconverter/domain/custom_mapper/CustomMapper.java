package org.silbertb.proto.domainconverter.domain.custom_mapper;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.domain.StringDomain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;

public class CustomMapper implements Mapper<CustomMapperTest, org.silbertb.proto.domainconverter.test.proto.CustomMapperTest> {
    @Override
    public CustomMapperTest toDomain(org.silbertb.proto.domainconverter.test.proto.CustomMapperTest protoValue) {
        CustomMapperTest domain = new CustomMapperTest();
        domain.setTcp(protoValue.getProtocol().equals(
                org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.Protocol.TCP));
        domain.setUdp(protoValue.getProtocol().equals(
                org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.Protocol.UDP));


        StringDomain stringDomain = ProtoDomainConverter.toDomain(protoValue.getStr());
        domain.setStringDomain(stringDomain);

        return domain;
    }

    @Override
    public org.silbertb.proto.domainconverter.test.proto.CustomMapperTest toProto(CustomMapperTest domainValue) {

        return org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.newBuilder()
                .setProtocol(domainValue.isTcp() ?
                        org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.Protocol.TCP :
                        org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.Protocol.UDP)
                .setStr(ProtoDomainConverter.toProto(domainValue.getStringDomain()))
                .build();
    }
}
