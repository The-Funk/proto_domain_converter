package org.silbertb.proto.domainconverter.domain.oneof.domain_field;


import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class OneofInfinityStartSegmentField implements OneofSegmentField {
    private final int start;

    @ProtoConstructor
    public OneofInfinityStartSegmentField(@ProtoField(protoName = "infinity_start") int start) {
        this.start = start;
    }
}
