package org.silbertb.proto.domainconverter.domain.multidomain.filesystem;

import org.silbertb.proto.domainconverter.custom.TypeConverter;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.multidomain.FileNodeMsg;

import java.util.List;
import java.util.stream.Collectors;

public class FileListConverter implements TypeConverter<List<FileNode>, FileNodeMsg.FileNodeList> {
    @Override
    public List<FileNode> toDomainValue(FileNodeMsg.FileNodeList protoValue) {
        return protoValue.getFilesList().stream().map(ProtoDomainConverter::toDomain).collect(Collectors.toUnmodifiableList());
    }

    @Override
    public boolean shouldAssignToProto(List<FileNode> domainValue) {
        return domainValue != null && !domainValue.isEmpty();
    }

    @Override
    public FileNodeMsg.FileNodeList toProtobufValue(List<FileNode> domainValue) {
        return FileNodeMsg.FileNodeList.newBuilder()
                .addAllFiles(domainValue.stream().map(ProtoDomainConverter::toProto).collect(Collectors.toUnmodifiableList()))
                .build();
    }
}
