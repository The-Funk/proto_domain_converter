package org.silbertb.proto.domainconverter.domain.builder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.CustomBuildMethodProto;

@ToString
@EqualsAndHashCode
@Builder(buildMethodName = "create")
@ProtoBuilder(buildMethodName = "create")
@ProtoClass(protoClass = CustomBuildMethodProto.class)
public class CustomBuildMethodDomain {
    @Getter
    @ProtoField
    private int intVal;
}
