package org.silbertb.proto.domainconverter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;

import java.util.LinkedList;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.ConcreteMessageList.class)
public class ConcreteMessageList {

    @ProtoField
    private LinkedList<PrimitiveDomain> messageList;
}
