package org.silbertb.proto.domainconverter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;

import java.util.List;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.MessageList.class)
public class MessageListDomain {

    @ProtoField
    private List<PrimitiveDomain> messageList;
}
