package org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentBuilderProto;

public class SegmentBuilderRangeMapper implements Mapper<OneofSegmentBuilderRange, OneofSegmentBuilderProto> {
    @Override
    public OneofSegmentBuilderRange toDomain(OneofSegmentBuilderProto protoValue) {
        String[] edges = protoValue.getRange().split("-");
        int start = Integer.parseInt(edges[0]);
        int end = Integer.parseInt(edges[1]);
        return OneofSegmentBuilderRange.builder()
                .name(protoValue.getName())
                .start(start)
                .end(end)
                .build();
    }

    @Override
    public OneofSegmentBuilderProto toProto(OneofSegmentBuilderRange domainValue) {
        return OneofSegmentBuilderProto.newBuilder()
                .setName(domainValue.getName())
                .setRange(domainValue.getStart()+"-"+ domainValue.getEnd())
                .build();
    }
}
