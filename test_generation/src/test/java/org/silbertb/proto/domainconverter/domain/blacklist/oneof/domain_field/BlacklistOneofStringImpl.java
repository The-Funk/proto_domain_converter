package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class BlacklistOneofStringImpl implements BlacklistOneof{
    private String stringValue;

    @EqualsAndHashCode.Exclude
    private String anotherValue;
}
