package org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_class;


import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.OneofImplHierarchyProto;


@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "val1", domainClass = OneofImplHierarchyImpl1.class),
        @OneofField(protoField = "val2", domainClass = OneofImplHierarchyImpl2.class)
})
@ProtoClass(protoClass = OneofImplHierarchyProto.class)
public interface OneofImplHierarchyDomain {
}
