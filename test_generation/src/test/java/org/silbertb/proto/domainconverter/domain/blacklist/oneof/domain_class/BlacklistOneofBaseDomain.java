package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_class;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.BlacklistOneofBaseProto;

@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "double_value", domainClass = BlacklistOneofDoubleImplDomain.class)
})
@ProtoClass(protoClass = BlacklistOneofBaseProto.class, blacklist = true)
@Data
public class BlacklistOneofBaseDomain {
    private long longValue;
}
