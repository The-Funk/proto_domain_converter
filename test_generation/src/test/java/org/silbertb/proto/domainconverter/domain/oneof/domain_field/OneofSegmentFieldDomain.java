package org.silbertb.proto.domainconverter.domain.oneof.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentFieldProto;


@ProtoClass(protoClass = OneofSegmentFieldProto.class)
@Data
public class OneofSegmentFieldDomain {
    @ProtoField
    private String name;

    @OneofBase(oneofName = "value", oneOfFields = {
            @OneofField(protoField = "point", domainClass = OneofSegmentFieldPoint.class),
            @OneofField(protoField = "open_range", domainClass = OneofSegmentFieldOpenRange.class, domainField = "range"),
            @OneofField(protoField = "infinity_start", domainClass = OneofInfinityStartSegmentField.class),
            @OneofField(protoField = "infinity_end", domainClass = OneofInfinityEndSegmentField.class),
            @OneofField(protoField = "empty_range", domainClass = OneofSegmentFieldEmptyRange.class),
            @OneofField(protoField = "range", domainClass = OneofSegmentFieldRange.class)
    })
    private OneofSegmentField segment;
}
