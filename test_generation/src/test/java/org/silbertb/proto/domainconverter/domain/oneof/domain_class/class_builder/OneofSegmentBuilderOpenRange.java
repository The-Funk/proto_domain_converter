package org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.RangeDomain;

@EqualsAndHashCode(callSuper = true)
@ProtoBuilder
@Data
@SuperBuilder
public class OneofSegmentBuilderOpenRange extends OneofSegmentBuilderDomain {
    @ProtoField(protoName = "open_range")
    private final RangeDomain range;
}
