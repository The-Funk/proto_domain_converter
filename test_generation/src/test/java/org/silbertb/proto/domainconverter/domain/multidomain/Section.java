package org.silbertb.proto.domainconverter.domain.multidomain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.multidomain.SectionMsg;

import java.util.List;

@Data
@EqualsAndHashCode
@ProtoClass(protoClass = SectionMsg.class)
public class Section {
    @ProtoField
    private String header;

    @ProtoField
    private List<String> lines;
}
