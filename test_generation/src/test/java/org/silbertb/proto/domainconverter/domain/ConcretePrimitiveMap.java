package org.silbertb.proto.domainconverter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;

import java.util.TreeMap;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveMap.class)
public class ConcretePrimitiveMap {
    @ProtoField
    private TreeMap<Integer, Long> primitiveMap;
}
