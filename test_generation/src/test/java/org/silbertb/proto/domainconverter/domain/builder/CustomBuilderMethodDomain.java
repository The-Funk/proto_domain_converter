package org.silbertb.proto.domainconverter.domain.builder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.CustomBuilderMethodProto;

@ToString
@EqualsAndHashCode
@Builder(builderMethodName = "toBuilder")
@ProtoBuilder(builderMethodName = "toBuilder")
@ProtoClass(protoClass = CustomBuilderMethodProto.class)
public class CustomBuilderMethodDomain {
    @Getter
    @ProtoField
    private int intVal;
}
