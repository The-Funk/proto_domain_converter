package org.silbertb.proto.domainconverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.silbertb.proto.domainconverter.domain.AllInOne;
import org.silbertb.proto.domainconverter.domain.AllInOneConstructor;
import org.silbertb.proto.domainconverter.domain.ConcreteMapToMessage;
import org.silbertb.proto.domainconverter.domain.ConcreteMessageList;
import org.silbertb.proto.domainconverter.domain.ConcretePrimitiveList;
import org.silbertb.proto.domainconverter.domain.ConcretePrimitiveMap;
import org.silbertb.proto.domainconverter.domain.MapToMessage;
import org.silbertb.proto.domainconverter.domain.OneofWithoutInheritance;
import org.silbertb.proto.domainconverter.domain.PrimitiveList;
import org.silbertb.proto.domainconverter.domain.PrimitiveMap;
import org.silbertb.proto.domainconverter.domain.SimpleContainer;
import org.silbertb.proto.domainconverter.domain.StringList;
import org.silbertb.proto.domainconverter.domain.*;
import org.silbertb.proto.domainconverter.domain.blacklist.BlacklistDomain;
import org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_class.BlacklistOneofBaseDomain;
import org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_class.BlacklistOneofDoubleImplDomain;
import org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field.*;
import org.silbertb.proto.domainconverter.domain.builder.*;
import org.silbertb.proto.domainconverter.domain.custom_converter.CustomConverter;
import org.silbertb.proto.domainconverter.domain.custom_converter.CustomListConverter;
import org.silbertb.proto.domainconverter.domain.custom_converter.CustomMapConverter;
import org.silbertb.proto.domainconverter.domain.custom_converter.MultiCustomConvertersDomain;
import org.silbertb.proto.domainconverter.domain.custom_mapper.CustomMapperTest;
import org.silbertb.proto.domainconverter.domain.multidomain.Library;
import org.silbertb.proto.domainconverter.domain.multidomain.OneofHolder;
import org.silbertb.proto.domainconverter.domain.multidomain.filesystem.FileNode;
import org.silbertb.proto.domainconverter.domain.oneof.RangeDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean.OneofSegmentOpenRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean.OneofSegmentPoint;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean.OneofSegmentRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder.OneofSegmentBuilderOpenRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder.OneofSegmentBuilderPoint;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder.OneofSegmentBuilderRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor.OneofSegmentConstructorOpenRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor.OneofSegmentConstructorPoint;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor.OneofSegmentConstructorRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder.OneofSegmentConstructorBuilderOpenRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder.OneofSegmentConstructorBuilderPoint;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder.OneofSegmentConstructorBuilderRange;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.no_members.CircleDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.no_members.ShapeDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_field.*;
import org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_class.OneofImplHierarchyDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_class.OneofImplHierarchyImpl2;
import org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_field.OneofImplHierarchyFieldDomain;
import org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_field.OneofImplHierarchyFieldImpl2;
import org.silbertb.proto.domainconverter.domain.oneof.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.domain.oneof.field.OneofWithFieldInheritance;
import org.silbertb.proto.domainconverter.serializers.FilesystemMarshal;
import org.silbertb.proto.domainconverter.test.proto.*;
import org.silbertb.proto.domainconverter.test.proto.multidomain.FileNodeMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.LibraryMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.OneofHolderMsg;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class TestItemsCreator {
    static public org.silbertb.proto.domainconverter.test.proto.AllInOne createAllInOneProto() {
        return org.silbertb.proto.domainconverter.test.proto.AllInOne.newBuilder()
                .setBytesVal(createBytesProto())
                .setListVal(createMessageListProto())
                .setMapVal(createConcreteMapToMessageProto())
                .setOneof1IntVal(3)
                .setOneof2Primitives(createPrimitivesProto())
                .build();
    }

    static public AllInOne createAllInOneDomain() {
        AllInOne domain = new AllInOne();
        domain.setBytesVal(createBytesDomain());
        domain.setListVal(createMessageListDomain());
        domain.setMapVal(createConcreteMapToMessageDomain());
        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);
        domain.setValue1(oneofIntImplDomain);
        domain.setValue2(createPrimitiveDomain());

        return domain;
    }

    static public BytesDomain createBytesDomain() {
        BytesDomain domain = new BytesDomain();
        domain.setBytesValue(new byte[]{0x1b, 0x2b});
        return domain;
    }

    static public BytesProto createBytesProto() {
        return BytesProto.newBuilder().setBytesValue(ByteString.copyFrom(new byte[]{0x1b, 0x2b})).build();
    }

    static public OneofWithFieldInheritance createOneofWithInheritanceDomain() {
        OneofWithFieldInheritance domain = new org.silbertb.proto.domainconverter.domain.oneof.field.OneofWithFieldInheritance();
        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);
        domain.setValue(oneofIntImplDomain);
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance createOneofWithFieldInheritanceProto() {
        return org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance.newBuilder()
                .setIntVal(3)
                .build();
    }

    static public OneofWithoutInheritance createOneofWithoutInheritanceDomain() {
        OneofWithoutInheritance domain = new OneofWithoutInheritance();
        domain.setIntVal(3);
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance createOneofWithoutInheritanceProto() {
        return org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance.newBuilder()
                .setIntVal(3)
                .build();
    }

    static public ConcreteMapToMessage createConcreteMapToMessageDomain() {
        ConcreteMapToMessage domain = new ConcreteMapToMessage();
        domain.setMapToMessage(new HashMap<>(Map.of(
                "aa", createPrimitiveDomain(),
                "bb", createPrimitiveDomain())));

        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.ConcreteMapToMessage createConcreteMapToMessageProto() {
        return org.silbertb.proto.domainconverter.test.proto.ConcreteMapToMessage.newBuilder()
                .putMapToMessage("aa", createPrimitivesProto())
                .putMapToMessage("bb", createPrimitivesProto())
                .build();
    }

    static public ConcretePrimitiveMap createConcretePrimitiveMapDomain() {
        ConcretePrimitiveMap domain = new ConcretePrimitiveMap();
        domain.setPrimitiveMap(new TreeMap<>(Map.of(1, 2L, 3, 4L)));
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveMap createConcretePrimitiveMapProto() {
        return org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveMap.newBuilder()
                .putPrimitiveMap(1, 2)
                .putPrimitiveMap(3, 4)
                .build();
    }

    static public OneofImplHierarchyDomain createOneofImplHierarchyDomain() {
        return new OneofImplHierarchyImpl2("aaa");
    }

    static public OneofImplHierarchyDomain createOneofImplHierarchyDomainSubclass() {
        return new OneofImplHierarchyImpl2("aaa"){};
    }

    static public OneofImplHierarchyProto createOneofImplHierarchyProto() {
        return OneofImplHierarchyProto.newBuilder().setVal2("aaa").build();
    }

    static public OneofImplHierarchyFieldDomain createOneofImplHierarchyFieldDomain() {
        OneofImplHierarchyFieldDomain oneofImplHierarchyFieldDomain = new OneofImplHierarchyFieldDomain();
        oneofImplHierarchyFieldDomain.setValue(new OneofImplHierarchyFieldImpl2("aaa"));
        return oneofImplHierarchyFieldDomain;
    }

    static public OneofImplHierarchyFieldProto createOneofImplHierarchyFieldProto() {
        return OneofImplHierarchyFieldProto.newBuilder().setVal2("aaa").build();
    }

    static public OneofImplHierarchyFieldDomain createOneofImplHierarchyFieldDomainSubclass() {
        OneofImplHierarchyFieldDomain oneofImplHierarchyFieldDomain = new OneofImplHierarchyFieldDomain();
        oneofImplHierarchyFieldDomain.setValue(new OneofImplHierarchyFieldImpl2("aaa"){});
        return oneofImplHierarchyFieldDomain;
    }

    static public MapToMessage createMapToMessageDomain() {
        MapToMessage domain = new MapToMessage();
        domain.setMapToMessage(Map.of(
                "aa", createPrimitiveDomain(),
                "bb", createPrimitiveDomain()));
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.MapToMessage createMapToMessageProto() {
        return org.silbertb.proto.domainconverter.test.proto.MapToMessage.newBuilder()
                .putMapToMessage("aa", createPrimitivesProto())
                .putMapToMessage("bb", createPrimitivesProto())
                .build();
    }

    static public PrimitiveMap createPrimitiveMapDomain() {
        PrimitiveMap domain = new PrimitiveMap();
        domain.setPrimitiveMap(Map.of(1, 2L, 3, 4L));
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.PrimitiveMap createPrimitiveMapProto() {
        return org.silbertb.proto.domainconverter.test.proto.PrimitiveMap.newBuilder()
                .putPrimitiveMap(1, 2)
                .putPrimitiveMap(3, 4)
                .build();
    }

    static public PrimitiveDomain createPrimitiveDomain() {
        PrimitiveDomain primitiveDomain = new PrimitiveDomain();
        primitiveDomain.setBooleanValue(true);
        primitiveDomain.setFloatValue(-0.1f);
        primitiveDomain.setDoubleValue(-0.5);
        primitiveDomain.setIntValue(-1);
        primitiveDomain.setLongValue(-2L);

        return primitiveDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.Primitives createPrimitivesProto() {
        return org.silbertb.proto.domainconverter.test.proto.Primitives.newBuilder()
                .setBooleanValue(true)
                .setFloatValue(-0.1f)
                .setDoubleValue(-0.5)
                .setIntValue(-1)
                .setLongValue(-2L)
                .build();
    }

    static public StringDomain createStringDomain() {
        StringDomain stringDomain = new StringDomain();
        stringDomain.setStringValue("aaaa");
        return stringDomain;
    }

    static public StringProto createStringProto() {
        return StringProto.newBuilder().setStringValue("aaaa").build();
    }

    static public SimpleContainer createSimpleContainerDomain() {
        SimpleContainer simpleContainerDomain = new SimpleContainer();
        simpleContainerDomain.setPrimitives(createPrimitiveDomain());

        return simpleContainerDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.SimpleContainer createSimpleContainerProto() {
        return org.silbertb.proto.domainconverter.test.proto.SimpleContainer.newBuilder()
                .setPrimitives(createPrimitivesProto())
                .build();
    }

    static public PrimitiveList createPrimitiveListDomain() {
        PrimitiveList listDomain = new PrimitiveList();
        listDomain.setIntList(new ArrayList<>(List.of(1, 2, 3)));
        return listDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.PrimitiveList createPrimitiveListProto() {
        return org.silbertb.proto.domainconverter.test.proto.PrimitiveList.newBuilder().addAllIntList(List.of(1, 2, 3)).build();
    }

    static public ConcretePrimitiveList createConcretePrimitiveListDomain() {
        ConcretePrimitiveList listDomain = new ConcretePrimitiveList();
        listDomain.setIntList(new LinkedList<>(List.of(1, 2, 3)));
        return listDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveList createConcretePrimitiveListProto() {
        return org.silbertb.proto.domainconverter.test.proto.ConcretePrimitiveList.newBuilder().addAllIntList(List.of(1, 2, 3)).build();
    }

    static public StringList createStringListDomain() {
        StringList listDomain = new StringList();
        listDomain.setStringList(new ArrayList<>(List.of("aa", "bb", "cc")));
        return listDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.StringList createStringListProto() {
        return org.silbertb.proto.domainconverter.test.proto.StringList.newBuilder().addAllStringList(List.of("aa", "bb", "cc")).build();
    }

    static public MessageListDomain createMessageListDomain() {
        MessageListDomain listDomain = new MessageListDomain();
        listDomain.setMessageList(new ArrayList<>(List.of(createPrimitiveDomain(), createPrimitiveDomain())));
        return listDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.MessageList createMessageListProto() {
        return org.silbertb.proto.domainconverter.test.proto.MessageList.newBuilder()
                .addMessageList(createPrimitivesProto())
                .addMessageList(createPrimitivesProto())
                .build();
    }

    static public ConcreteMessageList createConcreteMessageListDomain() {
        ConcreteMessageList listDomain = new ConcreteMessageList();
        listDomain.setMessageList(new LinkedList<>(List.of(createPrimitiveDomain(), createPrimitiveDomain())));
        return listDomain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.ConcreteMessageList createConcreteMessageListProto() {
        return org.silbertb.proto.domainconverter.test.proto.ConcreteMessageList.newBuilder()
                .addMessageList(createPrimitivesProto())
                .addMessageList(createPrimitivesProto())
                .build();
    }

    static public CustomConverter createCustomConverterDomain() {
        CustomConverter domain = new CustomConverter();
        domain.setStrVal("5");
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.CustomConverter createCustomConverterProto() {
        return org.silbertb.proto.domainconverter.test.proto.CustomConverter.newBuilder().setIntVal(5).build();
    }

    static public CustomListConverter createCustomListConverterDomain() {
        CustomListConverter domain = new CustomListConverter();
        domain.setCommaSeparatedInt("5,6");
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.CustomListConverter createCustomListConverterProto() {
        return org.silbertb.proto.domainconverter.test.proto.CustomListConverter.newBuilder().addIntList(5).addIntList(6).build();
    }

    static public CustomMapConverter createCustomMapConverterDomain() {
        CustomMapConverter domain = new CustomMapConverter();
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");

        domain.setMap(map);
        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.CustomMapConverter createCustomMapConverterProto() {
        return org.silbertb.proto.domainconverter.test.proto.CustomMapConverter.newBuilder().putIntMap(1, 2).putIntMap(3, 4).build();
    }

    static public MultiCustomConvertersDomain createMultiCustomConvertersDomain() {
        MultiCustomConvertersDomain domain = new MultiCustomConvertersDomain();
        domain.setStrVal("5");
        domain.setCommaSeparatedInt("5,6");
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");

        domain.setMap(map);

        return domain;
    }

    static public MultiCustomConvertersProto createMultiCustomConvertersProto() {
        return MultiCustomConvertersProto.newBuilder()
                .setIntVal(5)
                .addIntList(5).addIntList(6)
                .putIntMap(1, 2).putIntMap(3, 4)
                .build();
    }

    static public CustomMapperTest createCustomMapperDomain() {
        CustomMapperTest domain = new CustomMapperTest();
        domain.setUdp(true);
        StringDomain stringDomain = new StringDomain();
        stringDomain.setStringValue("aaa");
        domain.setStringDomain(stringDomain);

        return domain;
    }

    static public org.silbertb.proto.domainconverter.test.proto.CustomMapperTest createCustomMapperProto() {
        return org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.newBuilder()
                .setProtocol(org.silbertb.proto.domainconverter.test.proto.CustomMapperTest.Protocol.UDP)
                .setStr(StringProto.newBuilder().setStringValue("aaa"))
                .build();
    }

    static public org.silbertb.proto.domainconverter.test.proto.AllInOneConstructor createAllInOneConstructorProto() {
        return org.silbertb.proto.domainconverter.test.proto.AllInOneConstructor.newBuilder()
                .setStrVal(createStringProto())
                .setBytesVal(createBytesProto())
                .setListVal(createMessageListProto())
                .setMapVal(createConcreteMapToMessageProto())
                .setOneof1IntVal(3)
                .setOneof2Primitives(createPrimitivesProto())
                .build();
    }

    static public AllInOneConstructor createAllInOneConstructorDomain() {
        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);
        return new AllInOneConstructor(
                createStringDomain(),
                createBytesDomain(),
                createConcreteMapToMessageDomain(),
                createMessageListDomain(),
                oneofIntImplDomain,
                createPrimitiveDomain()
        );
    }

    static public AllInOneBuilderProto createAllInOneBuilderProto() {
        return AllInOneBuilderProto.newBuilder()
                .setBytesVal(ByteString.copyFrom(new byte[]{0x1b, 0x2b}))
                .setStrVal("ccc")
                .addAllListVal(List.of("aaa", "bbb"))
                .putAllMapVal(Map.of("key1", "val1", "key2", "val2"))
                .putMapVal("key2", "val2")
                .setOneofIntVal(3)
                .build();
    }

    static public AllInOneBuilderDomain createAllInOneBuilderDomain() {
        var domainBuilder = AllInOneBuilderDomain.builder()
                .bytesVal(new byte[]{0x1b, 0x2b})
                .strVal("ccc")
                .listVal(List.of("aaa", "bbb"))
                .mapVal(new HashMap<>(Map.of("key1", "val1", "key2", "val2")));

        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);

        return domainBuilder
                .value(oneofIntImplDomain)
                .build();
    }

    static public CustomBuilderMethodProto createCustomBuilderMethodProto() {
        return CustomBuilderMethodProto.newBuilder()
                .setIntVal(1)
                .build();
    }

    static public CustomBuilderMethodDomain createCustomerBuilderMethodDomain() {
        return CustomBuilderMethodDomain.toBuilder().intVal(1).build();
    }

    static public CustomBuildMethodProto createCustomBuildMethodProto() {
        return CustomBuildMethodProto.newBuilder()
                .setIntVal(1)
                .build();
    }

    static public CustomBuildMethodDomain createCustomerBuildMethodDomain() {
        return CustomBuildMethodDomain.builder().intVal(1).create();
    }

    static public BuilderCustomSetterPrefixProto createBuilderCustomSetterPrefixProto() {
        return BuilderCustomSetterPrefixProto.newBuilder()
                .setIntVal(1)
                .build();
    }

    static public BuilderCustomSetterPrefixDomain createBuilderCustomSetterPrefixDomain() {
        return BuilderCustomSetterPrefixDomain.builder().withIntVal(1).build();
    }

    static public AllInOneBuilderConstructorProto createAllInOneBuilderConstructorProto() {
        return AllInOneBuilderConstructorProto.newBuilder()
                .setBytesVal(ByteString.copyFrom(new byte[]{0x1b, 0x2b}))
                .setStrVal("ccc")
                .addAllListVal(List.of("aaa", "bbb"))
                .putAllMapVal(Map.of("key1", "val1", "key2", "val2"))
                .putMapVal("key2", "val2")
                .setOneofIntVal(3)
                .build();
    }

    static public AllInOneBuilderConstructorDomain createAllInOneBuilderConstructorDomain() {
        var domainBuilder = AllInOneBuilderConstructorDomain.builder()
                .bytesVal(new byte[]{0x1b, 0x2b})
                .strVal("ccc")
                .listVal(List.of("aaa", "bbb"))
                .mapVal(new HashMap<>(Map.of("key1", "val1", "key2", "val2")));

        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);

        return domainBuilder
                .value(oneofIntImplDomain)
                .build();
    }

    static public OneofSegmentPoint createOneofSegmentPointDomain() {
        OneofSegmentPoint oneofSegmentPoint = new OneofSegmentPoint();
        oneofSegmentPoint.setValue("1");
        oneofSegmentPoint.setName("Point");

        return oneofSegmentPoint;
    }

    static public OneofSegmentProto createOneofSegmentPointProto() {
        return OneofSegmentProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    static public OneofSegmentOpenRange createOneofSegmentOpenRangeDomain() {
        OneofSegmentOpenRange oneofSegmentOpenRange = new OneofSegmentOpenRange();
        oneofSegmentOpenRange.setRange(RangeDomain.builder().start(1).end(3).build());
        oneofSegmentOpenRange.setName("Open Range");

        return oneofSegmentOpenRange;
    }

    static public OneofSegmentProto createOneofSegmentOpenRangeProto() {
        return OneofSegmentProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    static public OneofSegmentRange createOneofSegmentRangeDomain() {
        OneofSegmentRange oneofSegmentRange = new OneofSegmentRange(1, 3);
        oneofSegmentRange.setName("Range");

        return oneofSegmentRange;
    }

    static public OneofSegmentProto createOneofSegmentRangeProto() {
        return OneofSegmentProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    static public OneofSegmentBuilderPoint createOneofSegmentBuilderPointDomain() {
        return OneofSegmentBuilderPoint.builder()
                .name("Point")
                .value("1")
                .build();
    }

    static public OneofSegmentBuilderProto createOneofSegmentBuilderPointProto() {
        return OneofSegmentBuilderProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    static public OneofSegmentBuilderOpenRange createOneofSegmentBuilderOpenRangeDomain() {
        return OneofSegmentBuilderOpenRange.builder()
                .range(RangeDomain.builder().start(1).end(3).build())
                .name("Open Range")
                .build();
    }

    static public OneofSegmentBuilderProto createOneofSegmentBuilderOpenRangeProto() {
        return OneofSegmentBuilderProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    static public OneofSegmentBuilderRange createOneofSegmentBuilderRangeDomain() {
        return OneofSegmentBuilderRange.builder()
                .start(1)
                .end(3)
                .name("Range")
                .build();
    }

    static public OneofSegmentBuilderProto createOneofSegmentBuilderRangeProto() {
        return OneofSegmentBuilderProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    static public OneofSegmentConstructorPoint createOneofSegmentConstructorPointDomain() {
        return new OneofSegmentConstructorPoint("Point", "1");
    }

    static public OneofSegmentConstructorProto createOneofSegmentConstructorPointProto() {
        return OneofSegmentConstructorProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    static public OneofSegmentConstructorOpenRange createOneofSegmentConstructorOpenRangeDomain() {
        return new OneofSegmentConstructorOpenRange("Open Range",
                RangeDomain.builder().start(1).end(3).build());
    }

    static public OneofSegmentConstructorProto createOneofSegmentConstructorOpenRangeProto() {
        return OneofSegmentConstructorProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    static public OneofSegmentConstructorRange createOneofSegmentConstructorRangeDomain() {
        return new OneofSegmentConstructorRange("Range", 1, 3);
    }

    static public OneofSegmentConstructorProto createOneofSegmentConstructorRangeProto() {
        return OneofSegmentConstructorProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    static public OneofSegmentConstructorBuilderPoint createOneofSegmentConstructorBuilderPointDomain() {
        return OneofSegmentConstructorBuilderPoint.builder().name("Point").value("1").build();
    }

    static public OneofSegmentConstructorBuilderProto createOneofSegmentConstructorBuilderPointProto() {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    static public OneofSegmentConstructorBuilderOpenRange createOneofSegmentConstructorBuilderOpenRangeDomain() {
        return OneofSegmentConstructorBuilderOpenRange.builder()
                .name("Open Range")
                .range(RangeDomain.builder().start(1).end(3).build())
                .build();
    }

    static public OneofSegmentConstructorBuilderProto createOneofSegmentConstructorBuilderOpenRangeProto() {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    static public OneofSegmentConstructorBuilderRange createOneofSegmentConstructorBuilderRangeDomain() {
        return OneofSegmentConstructorBuilderRange.builder().name("Range").start(1).end(3).build();
    }

    static public OneofSegmentConstructorBuilderProto createOneofSegmentConstructorBuilderRangeProto() {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    static public OneofSegmentFieldDomain createOneofSegmentFieldPointDomain() {
        OneofSegmentFieldPoint oneofSegmentFieldPoint = new OneofSegmentFieldPoint();
        oneofSegmentFieldPoint.setValue("1");

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Point");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldPoint);
        return oneofSegmentFieldDomain;
    }

    static public OneofSegmentFieldProto createOneofSegmentFieldPointProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    static public OneofSegmentFieldDomain createOneofSegmentFieldRangeDomain() {
        OneofSegmentFieldRange oneofSegmentFieldRange = new OneofSegmentFieldRange(1, 3);

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Range");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldRange);
        return oneofSegmentFieldDomain;
    }

    static public OneofSegmentFieldProto createOneofSegmentFieldRangeProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    static public OneofSegmentFieldDomain createOneofSegmentFieldInfinityStartDomain() {
        OneofInfinityStartSegmentField oneofSegmentFieldRange = new OneofInfinityStartSegmentField(1);

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Infinity Start");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldRange);
        return oneofSegmentFieldDomain;
    }

    static public OneofSegmentFieldProto createOneofSegmentFieldInfinityStartProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setInfinityStart(1)
                .setName("Infinity Start")
                .build();
    }

    static public OneofSegmentFieldDomain createOneofSegmentFieldEmptyRangeDomain() {
        OneofSegmentFieldEmptyRange oneofSegmentFieldEmptyRange = new OneofSegmentFieldEmptyRange();

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Empty Range");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldEmptyRange);
        return oneofSegmentFieldDomain;
    }

    static public OneofSegmentFieldProto createOneofSegmentFieldEmptyRangeProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setEmptyRange(EmptyRangeProto.newBuilder().build())
                .setName("Empty Range")
                .build();
    }

    static public OneofSegmentFieldDomain createOneofSegmentFieldInfinityEndDomain() {
        OneofInfinityEndSegmentField oneofSegmentFieldRange = new OneofInfinityEndSegmentField(1);

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Infinity End");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldRange);
        return oneofSegmentFieldDomain;
    }

    static public OneofSegmentFieldProto createOneofSegmentFieldInfinityEndProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setInfinityEnd(1)
                .setName("Infinity End")
                .build();
    }

    static public OneofSegmentFieldDomain createOneofSegmentFieldOpenRangeDomain() {
        OneofSegmentFieldOpenRange oneofSegmentFieldOpenRange = OneofSegmentFieldOpenRange
                .builder()
                .range(RangeDomain.builder().start(1).end(3).build())
                .build();

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Open Range");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldOpenRange);
        return oneofSegmentFieldDomain;
    }

    static public OneofSegmentFieldProto createOneofSegmentFieldOpenRangeProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    static public ShapeDomain createShapeDomain() {
        return new CircleDomain(3);
    }

    static public ShapeProto createShapeProto() {
        return ShapeProto.newBuilder()
                .setCircle(CircleProto.newBuilder().setRadius(3))
                .build();
    }

    static public BlacklistDomain createBlacklistDomain() {
        BlacklistDomain domain = new BlacklistDomain();
        domain.setIntValue("1");
        domain.setLongVal(2);
        domain.setDoubleValue(3.3);
        domain.setAnotherField("Another Value");

        return domain;
    }

    static public BlacklistProto createBlacklistProto() {
        return BlacklistProto.newBuilder()
                .setIntValue(1)
                .setLongValue(2)
                .setDoubleValue(3.3)
                .build();
    }

    static public BlacklistOneofFieldDomain createBlacklistOneofFieldStringDomain() {
        BlacklistOneofStringImpl stringImpl = new BlacklistOneofStringImpl();
        stringImpl.setStringValue("value");
        stringImpl.setAnotherValue(" another value");

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(stringImpl);

        return domain;
    }

    static public BlacklistOneofFieldProto createBlacklistOneofFieldStringProto() {
        return BlacklistOneofFieldProto.newBuilder().setStringValue("value").build();
    }

    static public BlacklistOneofFieldDomain createBlacklistOneofFieldBoolDomain() {
        BlacklistOneofBoolImpl boolImpl = new BlacklistOneofBoolImpl();
        boolImpl.setBool(true);

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(boolImpl);

        return domain;
    }

    static public BlacklistOneofFieldProto createBlacklistOneofFieldBoolProto() {
        return BlacklistOneofFieldProto.newBuilder().setBooleanValue(true).build();
    }

    static public BlacklistOneofFieldDomain createBlacklistOneofFieldBlacklistMsgDomain() {
        BlacklistOneofImplDomain msgImpl = new BlacklistOneofImplDomain();
        msgImpl.setStringValue("value");
        msgImpl.setAnotherValue(" another value");

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(msgImpl);

        return domain;
    }

    static public BlacklistOneofFieldProto createBlacklistOneofFieldBlacklistMsgProto() {
        return BlacklistOneofFieldProto.newBuilder()
                .setBlacklistMsg(BlacklistOneofImplProto.newBuilder().setStringValue("value"))
                .build();
    }

    static public BlacklistOneofFieldDomain createBlacklistOneofFieldWhitelistMsgDomain() {
        WhitelistOneofImplDomain msgImpl = new WhitelistOneofImplDomain();
        msgImpl.setStr("value");
        msgImpl.setAnotherValue(" another value");

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(msgImpl);

        return domain;
    }

    static public BlacklistOneofFieldProto createBlacklistOneofFieldWhitelistMsgProto() {
        return BlacklistOneofFieldProto.newBuilder()
                .setWhitelistMsg(WhitelistOneofImplProto.newBuilder().setStringValue("value"))
                .build();
    }

    static public BlacklistOneofFieldDomain createBlacklistOneofFieldInnerMsgDomain() {
        WhitelistOneofImplDomain msgImpl = new WhitelistOneofImplDomain();
        msgImpl.setStr("value");
        msgImpl.setAnotherValue(" another value");

        InnerMsgOneofImplDomain innerMsg = new InnerMsgOneofImplDomain();
        innerMsg.setInnerMsg(msgImpl);

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(innerMsg);

        return domain;
    }

    static public BlacklistOneofFieldProto createBlacklistOneofFieldInnerMsgProto() {
        return BlacklistOneofFieldProto.newBuilder()
                .setInnerMsg(WhitelistOneofImplProto.newBuilder().setStringValue("value"))
                .build();
    }

    static public BlacklistOneofBaseDomain createBlacklistOneofBaseDomain() {
        BlacklistOneofDoubleImplDomain domain = new BlacklistOneofDoubleImplDomain();
        domain.setLongValue(1);
        domain.setDoubleValue(1.1);

        return domain;
    }

    static public BlacklistOneofBaseProto createBlacklistOneofBaseProto() {
        return BlacklistOneofBaseProto.newBuilder().setLongValue(1).setDoubleValue(1.1).build();
    }

    @SneakyThrows
    static public LibraryMsg createLibraryProto() {
        LibraryMsg.Builder builder = LibraryMsg.newBuilder();
        parseProto(builder, "library.json");
        return builder.build();
    }

    @SneakyThrows
    static public Library createLibraryDomain() {
        Gson gson = new Gson();
        InputStream is = TestItemsCreator.class.getClassLoader().getResourceAsStream("library.json");
        assert is != null;
        return gson.fromJson(new InputStreamReader(is), Library.class);
    }

    static public OneofHolderMsg createOneofHolderProto(OneofHolderMsg.ValueCase valueCase) {
        final OneofHolderMsg.Builder builder = OneofHolderMsg.newBuilder();
        builder.setName("Name");
        switch (valueCase) {
            case X:
                builder.setX(OneofHolderMsg.A.newBuilder().setF(3.14159265359f).build());
                break;
            case Y:
                builder.setY(OneofHolderMsg.A.newBuilder().setF(2.71828182846f).build());
                break;
            case Z:
                builder.setZ(OneofHolderMsg.B.newBuilder().setI(1337).build());
                break;
            default:
                Assertions.fail("Unsupported value case: " + valueCase);
        }
        return builder.build();
    }

    static public OneofHolder createOneofHolderDomain(OneofHolderMsg.ValueCase valueCase) {
        final OneofHolder domain = new OneofHolder();
        domain.setName("Name");
        switch (valueCase) {
            case X:
                domain.setValue(new OneofHolder.X(3.14159265359f));
                break;
            case Y:
                domain.setValue(new OneofHolder.Y(2.71828182846f));
                break;
            case Z:
                domain.setValue(new OneofHolder.Z(1337));
                break;
            default:
                Assertions.fail("Unsupported value case: " + valueCase);
        }
        return domain;
    }

    static public FileNodeMsg createOneofBaseProto() {
        FileNodeMsg.Builder builder = FileNodeMsg.newBuilder();
        parseProto(builder, "filesystem_proto.json");
        return builder.build();
    }

    static public FileNode createOneofBaseDomain() {
        Gson gson = new GsonBuilder().registerTypeAdapter(FileNode.class, new FilesystemMarshal()).create();
        InputStream is = TestItemsCreator.class.getClassLoader().getResourceAsStream("filesystem_domain.json");
        assert is != null;
        return gson.fromJson(new InputStreamReader(is), FileNode.class);
    }

    @SneakyThrows
    static private void parseProto(Message.Builder builder, String fileName) {
        InputStream is = TestItemsCreator.class.getClassLoader().getResourceAsStream(fileName);
        assert is != null;
        JsonFormat.parser().ignoringUnknownFields().merge(new InputStreamReader(is), builder);
    }
}