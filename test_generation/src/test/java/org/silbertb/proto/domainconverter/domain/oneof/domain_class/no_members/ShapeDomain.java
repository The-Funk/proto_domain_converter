package org.silbertb.proto.domainconverter.domain.oneof.domain_class.no_members;

import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.ShapeProto;

@OneofBase(oneofName = "shape", oneOfFields = {
        @OneofField(protoField = "circle", domainClass = CircleDomain.class),
        @OneofField(protoField = "rectangle", domainClass = RectangleDomain.class)
})
@ProtoClass(protoClass = ShapeProto.class)
public interface ShapeDomain {
}
