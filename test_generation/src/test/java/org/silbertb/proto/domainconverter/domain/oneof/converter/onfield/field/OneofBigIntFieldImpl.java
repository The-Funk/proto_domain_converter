package org.silbertb.proto.domainconverter.domain.oneof.converter.onfield.field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.domain.oneof.converter.onfield.IntBigIntConverter;

import java.math.BigInteger;

@Data
public class OneofBigIntFieldImpl implements OneofBaseFieldWithConverter{
    @ProtoConverter(converter = IntBigIntConverter.class)
    private BigInteger bigIntVal;
}
