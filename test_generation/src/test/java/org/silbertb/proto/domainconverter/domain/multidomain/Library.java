package org.silbertb.proto.domainconverter.domain.multidomain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.multidomain.LibraryMsg;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Data
@EqualsAndHashCode
@ProtoClass(protoClass = LibraryMsg.class)
public class Library {
    @ProtoField
    private TreeMap<String, Article> articles;

    @ProtoField
    private List<Thesis> theses;

    @ProtoField
    private List<Proceeding> proceedings;
}
