package org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentBuilderProto;

@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "point", domainClass = OneofSegmentBuilderPoint.class, domainField = "value"),
        @OneofField(protoField = "open_range", domainClass = OneofSegmentBuilderOpenRange.class, domainField = "range"),
        @OneofField(protoField = "range", domainClass = OneofSegmentBuilderRange.class)
})
@ProtoClass(protoClass = OneofSegmentBuilderProto.class)
@Data
@SuperBuilder
public class OneofSegmentBuilderDomain {
    @ProtoField
    private final String name;
}
