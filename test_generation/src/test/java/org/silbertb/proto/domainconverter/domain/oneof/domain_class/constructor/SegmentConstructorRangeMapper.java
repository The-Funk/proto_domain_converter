package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentConstructorProto;

public class SegmentConstructorRangeMapper implements Mapper<OneofSegmentConstructorRange, OneofSegmentConstructorProto> {
    @Override
    public OneofSegmentConstructorRange toDomain(OneofSegmentConstructorProto protoValue) {
        String[] edges = protoValue.getRange().split("-");
        int start = Integer.parseInt(edges[0]);
        int end = Integer.parseInt(edges[1]);
        return new OneofSegmentConstructorRange(protoValue.getName(), start, end);
    }

    @Override
    public OneofSegmentConstructorProto toProto(OneofSegmentConstructorRange domainValue) {
        return OneofSegmentConstructorProto.newBuilder()
                .setName(domainValue.getName())
                .setRange(domainValue.getStart()+"-"+ domainValue.getEnd())
                .build();
    }
}
