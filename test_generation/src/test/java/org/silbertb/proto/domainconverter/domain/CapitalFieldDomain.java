package org.silbertb.proto.domainconverter.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.CapitalFieldProto;

@Data
@ProtoClass(protoClass = CapitalFieldProto.class)
public class CapitalFieldDomain {
    @ProtoField(protoName = "CAPITAL_FIELD")
    private String capitalFieldValue;
}
