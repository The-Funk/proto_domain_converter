package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor.OneofSegmentConstructorRange;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentConstructorBuilderProto;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentConstructorProto;

public class SegmentConstructorBuilderRangeMapper implements Mapper<OneofSegmentConstructorBuilderRange, OneofSegmentConstructorBuilderProto> {
    @Override
    public OneofSegmentConstructorBuilderRange toDomain(OneofSegmentConstructorBuilderProto protoValue) {
        String[] edges = protoValue.getRange().split("-");
        int start = Integer.parseInt(edges[0]);
        int end = Integer.parseInt(edges[1]);
        return OneofSegmentConstructorBuilderRange.builder()
                .name(protoValue.getName())
                .start(start)
                .end(end)
                .build();
    }

    @Override
    public OneofSegmentConstructorBuilderProto toProto(OneofSegmentConstructorBuilderRange domainValue) {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setName(domainValue.getName())
                .setRange(domainValue.getStart()+"-"+ domainValue.getEnd())
                .build();
    }
}
