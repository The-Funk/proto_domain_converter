package org.silbertb.proto.domainconverter.domain.oneof.domain_field;

import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;

@ProtoConverter(converter = SegmentRangeConverter.class)
@Value
public class OneofSegmentFieldRange implements OneofSegmentField {
    int start;
    int end;
}
