package org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.RangeDomain;

@EqualsAndHashCode(callSuper = true)
@Data
public class OneofSegmentOpenRange extends OneofSegmentDomain {
    @ProtoField(protoName = "open_range")
    private RangeDomain range;
}
