package org.silbertb.proto.domainconverter.domain.builder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.domain.oneof.field.OneofBaseFieldDomain;
import org.silbertb.proto.domainconverter.domain.oneof.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.test.proto.AllInOneBuilderConstructorProto;

import java.util.HashMap;
import java.util.List;

@ToString
@EqualsAndHashCode
@ProtoClass(protoClass = AllInOneBuilderConstructorProto.class)
public class AllInOneBuilderConstructorDomain {

    @Builder
    @ProtoBuilder
    private AllInOneBuilderConstructorDomain(String strVal, byte[] bytesVal, HashMap<String, String> mapVal, List<String> listVal,
                                            @OneofBase(oneOfFields = {
                                                    @OneofField(protoField = "oneof_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
                                                    @OneofField(protoField = "oneof_primitives", domainClass = PrimitiveDomain.class)
                                            })
                                            OneofBaseFieldDomain value) {
        this.strVal = strVal;
        this.bytesVal = bytesVal;
        this.mapVal = mapVal;
        this.listVal = listVal;
        this.value = value;
    }

    @Getter
    private final String strVal;

    @Getter
    private final byte[] bytesVal;

    @Getter
    private final HashMap<String, String> mapVal;

    @Getter
    private final List<String> listVal;

    @Getter
    private final OneofBaseFieldDomain value;
}
