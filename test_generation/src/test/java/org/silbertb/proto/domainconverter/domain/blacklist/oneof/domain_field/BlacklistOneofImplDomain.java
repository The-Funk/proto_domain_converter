package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoIgnore;
import org.silbertb.proto.domainconverter.test.proto.BlacklistOneofImplProto;

@Data
@ProtoClass(protoClass = BlacklistOneofImplProto.class, blacklist = true)
public class BlacklistOneofImplDomain implements BlacklistOneof {
    private String stringValue;

    @EqualsAndHashCode.Exclude
    @ProtoIgnore
    private String anotherValue;
}
