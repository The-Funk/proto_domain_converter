package org.silbertb.proto.domainconverter.domain.oneof;

import lombok.Builder;
import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.RangeProto;

@Data
@Builder
@ProtoClass(protoClass = RangeProto.class)
@ProtoBuilder
public class RangeDomain {
    @ProtoField
    private final int start;

    @ProtoField
    private final int end;
}
