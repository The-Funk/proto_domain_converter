package org.silbertb.proto.domainconverter.serializers;

import com.google.gson.*;
import org.silbertb.proto.domainconverter.domain.multidomain.filesystem.File;
import org.silbertb.proto.domainconverter.domain.multidomain.filesystem.FileNode;
import org.silbertb.proto.domainconverter.domain.multidomain.filesystem.Folder;

import java.lang.reflect.Type;

public class FilesystemMarshal implements JsonDeserializer<FileNode> {

    @Override
    public FileNode deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        if (jsonObject.has("files")) {
            return context.deserialize(json, Folder.class);
        } else {
            return context.deserialize(json, File.class);
        }
    }
}
