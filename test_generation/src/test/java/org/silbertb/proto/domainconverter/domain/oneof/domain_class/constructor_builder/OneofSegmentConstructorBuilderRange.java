package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;

@ProtoClassMapper(mapper = SegmentConstructorBuilderRangeMapper.class)
@EqualsAndHashCode(callSuper = true)
@Getter
public class OneofSegmentConstructorBuilderRange extends OneofSegmentConstructorBuilderDomain {
    private final int start;
    private final int end;

    @Builder
    private OneofSegmentConstructorBuilderRange(String name, int start, int end) {
        super(name);
        this.start = start;
        this.end = end;
    }
}
