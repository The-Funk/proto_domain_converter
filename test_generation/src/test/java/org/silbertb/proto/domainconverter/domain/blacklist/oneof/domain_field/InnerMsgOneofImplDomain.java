package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field;

import lombok.Data;

@Data
public class InnerMsgOneofImplDomain implements BlacklistOneof {
    private WhitelistOneofImplDomain innerMsg;
}
