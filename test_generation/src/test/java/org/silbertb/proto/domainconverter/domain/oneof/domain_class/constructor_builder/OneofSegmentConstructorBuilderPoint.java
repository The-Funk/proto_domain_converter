package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.IntStringConverter;

@EqualsAndHashCode(callSuper = true)
@Getter
public class OneofSegmentConstructorBuilderPoint extends OneofSegmentConstructorBuilderDomain {

    private final String value;

    @Builder
    @ProtoBuilder
    private OneofSegmentConstructorBuilderPoint(
            String name,
            @ProtoConverter(converter = IntStringConverter.class)
            @ProtoField(protoName = "point")
            String value) {
        super(name);
        this.value = value;
    }
}
