package org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_field;


import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.OneofImplHierarchyFieldProto;

@ProtoClass(protoClass = OneofImplHierarchyFieldProto.class)
@Data
public class OneofImplHierarchyFieldDomain {

    @OneofBase(oneOfFields = {
            @OneofField(protoField = "val1", domainClass = OneofImplHierarchyFieldImpl1.class),
            @OneofField(protoField = "val2", domainClass = OneofImplHierarchyFieldImpl2.class)
    })
    private OneofImplHierarchyField value;
}
