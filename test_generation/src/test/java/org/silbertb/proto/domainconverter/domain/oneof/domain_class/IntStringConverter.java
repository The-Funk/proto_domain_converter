package org.silbertb.proto.domainconverter.domain.oneof.domain_class;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

public class IntStringConverter implements TypeConverter<String, Integer> {
    @Override
    public String toDomainValue(Integer protoValue) {
        return protoValue.toString();
    }

    @Override
    public boolean shouldAssignToProto(String domainValue) {
        return domainValue != null && !domainValue.isEmpty();
    }

    @Override
    public Integer toProtobufValue(String domainValue) {
        return Integer.parseInt(domainValue);
    }
}
