package org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.IntStringConverter;

@EqualsAndHashCode(callSuper = true)
@Data
public class OneofSegmentPoint extends OneofSegmentDomain {

    @ProtoConverter(converter = IntStringConverter.class)
    @ProtoField(protoName = "point")
    private String value;
}
