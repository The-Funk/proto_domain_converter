package org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;

@ProtoClassMapper(mapper = SegmentRangeMapper.class)
@EqualsAndHashCode(callSuper = true)
@Value
public class OneofSegmentRange extends OneofSegmentDomain {
    int start;
    int end;
}
