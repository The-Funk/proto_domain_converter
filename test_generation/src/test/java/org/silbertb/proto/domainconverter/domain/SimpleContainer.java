package org.silbertb.proto.domainconverter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.SimpleContainer.class)
public class SimpleContainer {

    @ProtoField
    private PrimitiveDomain primitives;
}
