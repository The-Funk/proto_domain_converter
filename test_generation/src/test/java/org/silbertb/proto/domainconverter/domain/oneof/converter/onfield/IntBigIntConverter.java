package org.silbertb.proto.domainconverter.domain.oneof.converter.onfield;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

import java.math.BigInteger;

public class IntBigIntConverter implements TypeConverter<BigInteger, Integer> {

    @Override
    public BigInteger toDomainValue(Integer protoValue) {
        return BigInteger.valueOf(protoValue.longValue());
    }

    @Override
    public boolean shouldAssignToProto(BigInteger domainValue) {
        return domainValue != null;
    }

    @Override
    public Integer toProtobufValue(BigInteger domainValue) {
        return domainValue.intValue();
    }
}
