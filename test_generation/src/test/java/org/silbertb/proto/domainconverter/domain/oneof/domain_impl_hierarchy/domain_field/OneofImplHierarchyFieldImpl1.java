package org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class OneofImplHierarchyFieldImpl1 implements OneofImplHierarchyField {

    private final String val;

    @ProtoConstructor
    public OneofImplHierarchyFieldImpl1(@ProtoField(protoName = "val1") String val) {
        this.val = val;
    }
}
