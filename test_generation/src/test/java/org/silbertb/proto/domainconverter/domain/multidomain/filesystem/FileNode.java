package org.silbertb.proto.domainconverter.domain.multidomain.filesystem;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.multidomain.FileNodeMsg;

@Data
@ProtoClass(protoClass = FileNodeMsg.class)
@OneofBase(oneofName = "content", oneOfFields = {
        @OneofField(protoField = "children", domainClass = Folder.class),
        @OneofField(protoField = "lines", domainClass = File.class),
})
public abstract class FileNode {
    @ProtoField
    private String name;
}
