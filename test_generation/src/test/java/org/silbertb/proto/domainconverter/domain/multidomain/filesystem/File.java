package org.silbertb.proto.domainconverter.domain.multidomain.filesystem;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@EqualsAndHashCode(callSuper = true)
@Data
public class File extends FileNode {
    @ProtoField
    private String lines;
}
