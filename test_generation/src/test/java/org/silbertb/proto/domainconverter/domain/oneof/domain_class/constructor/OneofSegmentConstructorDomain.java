package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentConstructorProto;

@OneofBase(oneofName = "value", oneOfFields = {
        @OneofField(protoField = "point", domainClass = OneofSegmentConstructorPoint.class, domainField = "value"),
        @OneofField(protoField = "open_range", domainClass = OneofSegmentConstructorOpenRange.class, domainField = "range"),
        @OneofField(protoField = "range", domainClass = OneofSegmentConstructorRange.class)
})
@ProtoClass(protoClass = OneofSegmentConstructorProto.class)
@EqualsAndHashCode
@Getter
public class OneofSegmentConstructorDomain {
    private final String name;

    public OneofSegmentConstructorDomain(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
