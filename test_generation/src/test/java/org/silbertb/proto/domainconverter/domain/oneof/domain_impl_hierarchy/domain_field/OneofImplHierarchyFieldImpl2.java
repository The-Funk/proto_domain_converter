package org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_field;

import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

public class OneofImplHierarchyFieldImpl2 extends OneofImplHierarchyFieldImpl1 {

    @ProtoConstructor
    public OneofImplHierarchyFieldImpl2(@ProtoField(protoName = "val2") String val) {
        super(val);
    }
}
