package org.silbertb.proto.domainconverter.domain.oneof.converter.onclass;

import lombok.Data;

@Data
public class Point implements SegmentDomain {
    private int value;
}
