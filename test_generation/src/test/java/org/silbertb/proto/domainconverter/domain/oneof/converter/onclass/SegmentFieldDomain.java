package org.silbertb.proto.domainconverter.domain.oneof.converter.onclass;


import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.SimpleSegmentFieldProto;

@Data
@ProtoClass(protoClass = SimpleSegmentFieldProto.class)
public class SegmentFieldDomain {
    @OneofBase(oneOfFields = {
            @OneofField(protoField = "point", domainClass = Point.class, domainField = "value"),
            @OneofField(protoField = "range", domainClass = Range.class)
    })
    private SegmentDomain segment;

}
