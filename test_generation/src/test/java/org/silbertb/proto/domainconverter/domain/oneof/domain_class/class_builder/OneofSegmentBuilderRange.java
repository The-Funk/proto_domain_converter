package org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;

@ProtoClassMapper(mapper = SegmentBuilderRangeMapper.class)
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class OneofSegmentBuilderRange extends OneofSegmentBuilderDomain {
    private final int start;
    private final int end;
}
