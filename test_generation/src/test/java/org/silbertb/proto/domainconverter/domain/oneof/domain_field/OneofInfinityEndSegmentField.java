package org.silbertb.proto.domainconverter.domain.oneof.domain_field;

import lombok.Builder;
import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class OneofInfinityEndSegmentField implements OneofSegmentField{
    private int end;

    @ProtoBuilder
    @Builder
    public OneofInfinityEndSegmentField(@ProtoField(protoName = "infinityEnd") int end) {
        this.end = end;
    }
}
