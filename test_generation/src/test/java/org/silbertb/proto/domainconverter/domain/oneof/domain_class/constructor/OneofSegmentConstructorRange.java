package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;

@ProtoClassMapper(mapper = SegmentConstructorRangeMapper.class)
@EqualsAndHashCode(callSuper = true)
@Getter
public class OneofSegmentConstructorRange extends OneofSegmentConstructorDomain {

    private final int start;
    private final int end;

    public OneofSegmentConstructorRange(String name, int start, int end) {
        super(name);
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
