package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor_builder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.RangeDomain;

@EqualsAndHashCode(callSuper = true)
@Getter
public class OneofSegmentConstructorBuilderOpenRange extends OneofSegmentConstructorBuilderDomain {

    private final RangeDomain range;

    @Builder
    @ProtoBuilder
    private OneofSegmentConstructorBuilderOpenRange(
            String name,
            @ProtoField(protoName = "open_range") RangeDomain range) {
        super(name);
        this.range = range;
    }
}
