package org.silbertb.proto.domainconverter.domain.multidomain.filesystem;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Folder extends FileNode {
    @ProtoField(protoName = "children")
    @ProtoConverter(converter = FileListConverter.class)
    private List<FileNode> files;
}
