package org.silbertb.proto.domainconverter.domain.oneof.domain_class.no_members;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.RectangleProto;
import org.silbertb.proto.domainconverter.test.proto.ShapeProto;

@ProtoClass(protoClass = RectangleProto.class)
@EqualsAndHashCode
@Getter
public class RectangleDomain implements ShapeDomain {

    private final double a;
    private final double b;

    @ProtoConstructor
    public RectangleDomain(@ProtoField(protoName = "width") double a, @ProtoField(protoName = "length") double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

}
