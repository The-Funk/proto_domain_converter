package org.silbertb.proto.domainconverter.domain.oneof.domain_impl_hierarchy.domain_class;

import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

public class OneofImplHierarchyImpl2 extends OneofImplHierarchyImpl1 {

    @ProtoConstructor
    public OneofImplHierarchyImpl2(@ProtoField(protoName = "val2") String val) {
        super(val);
    }
}
