package org.silbertb.proto.domainconverter.domain.oneof.domain_class.class_builder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.domain_class.IntStringConverter;

@ProtoBuilder
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class OneofSegmentBuilderPoint extends OneofSegmentBuilderDomain {

    @ProtoConverter(converter = IntStringConverter.class)
    @ProtoField(protoName = "point")
    private final String value;

}
