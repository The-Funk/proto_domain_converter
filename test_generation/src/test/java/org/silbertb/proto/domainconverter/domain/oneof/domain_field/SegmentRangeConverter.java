package org.silbertb.proto.domainconverter.domain.oneof.domain_field;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

public class SegmentRangeConverter implements TypeConverter<OneofSegmentFieldRange, String> {
    @Override
    public OneofSegmentFieldRange toDomainValue(String protoValue) {
        String[] edges = protoValue.split("-");
        int start = Integer.parseInt(edges[0]);
        int end = Integer.parseInt(edges[1]);
        return new OneofSegmentFieldRange(start, end);
    }

    @Override
    public String toProtobufValue(OneofSegmentFieldRange domainValue) {
        return domainValue.getStart()+"-"+ domainValue.getEnd();
    }


    @Override
    public boolean shouldAssignToProto(OneofSegmentFieldRange domainValue) {
        return true;
    }


}
