package org.silbertb.proto.domainconverter.domain.blacklist.oneof.domain_field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class BlacklistOneofBoolImpl implements BlacklistOneof {

    @ProtoField(protoName = "boolean_value")
    private boolean bool;
}
