package org.silbertb.proto.domainconverter.domain.oneof.domain_class.constructor;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.domain.oneof.RangeDomain;

@EqualsAndHashCode(callSuper = true)
@Getter
public class OneofSegmentConstructorOpenRange extends OneofSegmentConstructorDomain{

    private final RangeDomain range;

    @ProtoConstructor
    public OneofSegmentConstructorOpenRange(
            String name,
            @ProtoField(protoName = "open_range") RangeDomain range) {
        super(name);
        this.range = range;
    }
}
