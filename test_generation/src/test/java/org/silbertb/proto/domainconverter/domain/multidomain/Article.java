package org.silbertb.proto.domainconverter.domain.multidomain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.multidomain.DocumentMsg;

@ProtoClass(protoClass = DocumentMsg.class, withInheritedFields = true)
public class Article extends Document {
}
