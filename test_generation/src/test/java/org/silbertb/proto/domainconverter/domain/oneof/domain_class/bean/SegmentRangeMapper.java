package org.silbertb.proto.domainconverter.domain.oneof.domain_class.bean;

import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.test.proto.OneofSegmentProto;

public class SegmentRangeMapper implements Mapper<OneofSegmentRange, OneofSegmentProto> {
    @Override
    public OneofSegmentRange toDomain(OneofSegmentProto protoValue) {
        String[] edges = protoValue.getRange().split("-");
        int start = Integer.parseInt(edges[0]);
        int end = Integer.parseInt(edges[1]);
        OneofSegmentRange range = new OneofSegmentRange(start, end);
        range.setName(protoValue.getName());
        return range;
    }

    @Override
    public OneofSegmentProto toProto(OneofSegmentRange domainValue) {
        return OneofSegmentProto.newBuilder()
                .setName(domainValue.getName())
                .setRange(domainValue.getStart()+"-"+ domainValue.getEnd())
                .build();
    }
}
