package org.silbertb.proto.domainconverter.domain.multidomain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.test.proto.multidomain.OneofHolderMsg;

@Data
@ProtoClass(protoClass = OneofHolderMsg.class)
@ProtoClassDefault
public class OneofHolder {
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ProtoClass(protoClass = OneofHolderMsg.A.class)
    public static class X {
        @ProtoField
        private float f;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ProtoClass(protoClass = OneofHolderMsg.A.class)
    public static class Y {
        @ProtoField
        private float f;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ProtoClass(protoClass = OneofHolderMsg.B.class)
    public static class Z {
        @ProtoField
        private int i;
    }

    @ProtoField
    private String name;

    @OneofBase(oneOfFields = {
            @OneofField(protoField = "x", domainClass = X.class),
            @OneofField(protoField = "y", domainClass = Y.class),
            @OneofField(protoField = "z", domainClass = Z.class),
    })
    private Object value;
}
